package fax.testovi;

import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.Click;

import istrazivaci.IstrazivaciLicniPodaci;
import istrazivaci.IstrazivaciMainPage;
import istrazivaci.IstrazivaciProjekatPage;
import minis.LoginPage;
import minis.MinisMenuPage;
import minis.ResetPassPage;

public class istrLicniPodaciDisp {
	private WebDriver driver;
	private LoginPage loginPage;
	private ResetPassPage resetPassPage;
	private MinisMenuPage minisMenuPage;
	private IstrazivaciMainPage istrazivaciMainPage;
	private IstrazivaciLicniPodaci istrazivaciLicniPodaci;
	private IstrazivaciProjekatPage istrazivaciProjekat;

	@BeforeSuite
	public void setUp(){
		//System.setProperty("webdriver.chrome.driver", "chromedriver");
		//driver = new ChromeDriver();
		driver = new FirefoxDriver();
		String baseUrl = "http://192.168.58.25:8080/";
		//String baseUrl = "http://park.ftn.uns.ac.rs:8080/";
		driver.manage().window().maximize();
		driver.get(baseUrl);
		loginPage = new LoginPage(driver);
		resetPassPage = new ResetPassPage(driver);
		minisMenuPage = new MinisMenuPage(driver);
		istrazivaciMainPage = new IstrazivaciMainPage(driver);
		istrazivaciLicniPodaci = new IstrazivaciLicniPodaci(driver);
		istrazivaciProjekat = new IstrazivaciProjekatPage(driver);
		
			}
	@AfterSuite
	public void closeAll(){
		minisMenuPage.getDropUserMenu().click();
		minisMenuPage.getLogOutBtn().click();
		driver.close();
	}
	@Test
	public void loginTestDisplay(){
	assertTrue(loginPage.getLoginTitle().isDisplayed());
	assertTrue(loginPage.getUsername().isDisplayed());
	assertTrue(loginPage.getPassword().isDisplayed());
	assertTrue(loginPage.getOdustaniBtn().isDisplayed());
	assertTrue(loginPage.getPrijaviSeBtn().isDisplayed());
	assertTrue(loginPage.getZaboravljenPass().isDisplayed());
	loginPage.loginFunc("dinorac87@gmail.com", "123456");
	}
	@Test(dependsOnMethods="loginTestDisplay")
	public void minisTestDisplay() throws InterruptedException{
		assertTrue(minisMenuPage.getMiniTitle().isDisplayed());
		assertTrue(minisMenuPage.getInstitucija().isDisplayed());
		assertTrue(minisMenuPage.getIstrazivaci().isDisplayed());
		minisMenuPage.getIstrazivaci().click();
		istrazivaciMainPage.getDodajIstrazivacaBtn().click();
	

		assertTrue(istrazivaciLicniPodaci.getCelaForma().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getDatumRodjenja().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getDrzavaBoravista().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getDrzavaRodjenja().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getEmail().isDisplayed());
	
		assertTrue(istrazivaciLicniPodaci.getImeRoditelja().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getJmbg() .isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getLicniPodaci().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getMestoBoravista().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getMestoRodjenja().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getOdustaniBtn().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getOpstinaBoravista().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getOpstinaRodjenja().isDisplayed());
	
		assertTrue(istrazivaciLicniPodaci.getSacuvajBtn().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getTelefon().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getUlicaIBroj().isDisplayed());
		assertTrue(istrazivaciLicniPodaci.getVebAdresa().isDisplayed());
		istrazivaciLicniPodaci.setIme("pera");
		istrazivaciLicniPodaci.getSacuvajBtn().click();
		
		driver.switchTo().alert().accept();
		//Thread.sleep(4000);
		istrazivaciLicniPodaci.setPrezime("peric");
		istrazivaciLicniPodaci.getPol().sendKeys("Ženski");
		assertTrue(!istrazivaciProjekat.getPodaciProjekat().isEnabled());
		
		
	}
	
}
