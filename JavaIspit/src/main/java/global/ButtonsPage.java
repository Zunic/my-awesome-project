package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ButtonsPage {
	private WebDriver driver;

	public ButtonsPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getCancelBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Odustani']")));
	}

	public WebElement getSignInBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Prijavi se']")));
	}

	public WebElement confirmPassChangeBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@name='btnChange']")));
	}

}
