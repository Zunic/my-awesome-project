package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LanguagesPage {
	private WebDriver driver;

	public LanguagesPage(WebDriver driver) {
		this.driver = driver;
	}	

	public WebElement getJeziciBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//div [@class='navbar-custom-menu']/ul[1]")));
	}

	public WebElement srpskiLat() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@translate='BUTTON_LANG_SR_LAT']")));
	}

	public WebElement srpskiCir() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@translate='BUTTON_LANG_SR_CYR']")));
	}

	public WebElement engleski() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@translate='BUTTON_LANG_EN']")));
	}

}
