package global;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getUsername() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("username")));
	}
	
	public void setUsername(String username) {
		WebElement usernameField = getUsername();
		usernameField.clear();
		usernameField.sendKeys(username);
	}
	
	public WebElement getPassword() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("password")));
	}
	
	public void setPassword(String username) {
		WebElement passwordField = getPassword();
		passwordField.clear();
		passwordField.sendKeys(username);
	}
	
	public WebElement getSignInBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Prijavi se']")));
	}

	public WebElement getCancelBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Odustani']")));
	}
	
	public WebElement forgotPassword() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@ui-sref='forgotPassword']")));
	}
	
	public void login (String username, String password) {
		setUsername(username);
		setPassword(password);
		getSignInBtn().click();
	}
	
	public String getError() {
		
		String errorMessage = null;
		List<WebElement> errors = driver.findElements(By.xpath("//span [@ng-repeat='v in validations']"));
		
		for (int i = 0; i < errors.size(); i++) {
			if (errors.get(i).isDisplayed()) {
				 errorMessage =  errors.get(i).getText();
			} 
		}
		return errorMessage; 
	}
	
	

	
}
