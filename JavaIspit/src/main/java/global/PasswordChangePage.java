package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PasswordChangePage {
	private WebDriver driver;

	public PasswordChangePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getStaraLozinka() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='oldPassword']")));
	}
	
	public void setStaraLozinka (String staraLozinka) {
		WebElement staraLozinkaField = getStaraLozinka();
		staraLozinkaField.clear();
		staraLozinkaField.sendKeys(staraLozinka);
	}
	
	public WebElement getNovaLozinka() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='newPassword']")));
	}
	
	public void setNovaLozinka (String novaLozinka) {
		WebElement novaLozinkaField = getNovaLozinka();
		novaLozinkaField.clear();
		novaLozinkaField.sendKeys(novaLozinka);
	}

	public WebElement getPonoviNovuLozinku() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='newPasswordRepeat']")));
	}
	
	public void setPonoviNovuLozinku (String ponovljenaLozinka) {
		WebElement ponovljenaLozinkaField = getPonoviNovuLozinku();
		ponovljenaLozinkaField.clear();
		ponovljenaLozinkaField.sendKeys(ponovljenaLozinka);
	}

}
