package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SideMenuPage {
	private WebDriver driver;

	public SideMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getInstitucijaBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//*[@id='main-page']/aside/section/ul/li[3]/a")));
	}

	public WebElement getIstrazivaciBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//*[@id='main-page']/aside/section/ul/li[4]/a")));
	}

}
