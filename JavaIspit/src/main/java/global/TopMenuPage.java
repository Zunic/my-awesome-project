package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TopMenuPage {
	private WebDriver driver;

	public TopMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getLogoMini() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@class='logo-mini']")));
	}

	public WebElement getLogoLarge() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@class='logo-lg']")));
	}

	public WebElement getSidebarToggle() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@class='sidebar-toggle']")));
	}

	public WebElement getJeziciBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//div [@class='navbar-custom-menu']/ul[1]")));
	}

	public WebElement getJezicUserMenuBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//div [@class='navbar-custom-menu']/ul[2]")));
	}

}
