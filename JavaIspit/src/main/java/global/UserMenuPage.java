package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserMenuPage {
	private WebDriver driver;

	public UserMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getUserMenuBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//div [@class='navbar-custom-menu']/ul[2]")));
	}
	
	public WebElement getUputstvoZaKoriscenje() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@translate='USER_MANUAL']")));
	}
	
	public WebElement getKontaktirajteNas() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@translate='USER_SUPPORT']")));
	}

	public WebElement getPromenaLozinke() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@translate='CHANGE_PASSWORD']")));
	}
	
	public WebElement getOdjava() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@translate='LOGOUT']")));
	}

}
