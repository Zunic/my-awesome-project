package institucija;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AngazujOsobuPage {
	private WebDriver driver;

	public AngazujOsobuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getOsoba() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@placeholder='Pretraži osobe']")));
	}
	
	public void setUsername(String osoba) {
		WebElement osobaField = getOsoba();
		osobaField.clear();
		osobaField.sendKeys(osoba);
	}

	public WebElement getZvanje() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@ng-model='connection.position']")));
	}
	
	public WebElement odaberiZvanje(String zvanje) {
		getZvanje().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + zvanje + "']")));
	}
	
	public WebElement getFunkcija() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@ng-model='connection.function']")));
	}
	
	public WebElement odaberiFunkciju(String funkcija) {
		getFunkcija().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + funkcija + "']")));
	}

	public WebElement getProcenatZaposlenosti() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='employmentPercentage']")));
	}
	
	public void setProcenatZaposlenosti(String procenat) {
		WebElement procenatField = getProcenatZaposlenosti();
		procenatField.clear();
		procenatField.sendKeys(procenat);
	}
	
	public WebElement getBrojMeseci() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@ng-model='cconnection.maxMonthEngagement']")));
	}
	
	public WebElement odaberiBrojMeseci(String brojMeseci) {
		getBrojMeseci().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + brojMeseci + "']")));
	}

	public WebElement getOd() {
		List<WebElement> dates = driver.findElements
				(By.xpath("//input [@ng-model='data.date']"));
		return dates.get(4);
	}
	
	public void setOd(String datumOd) {
		WebElement odField = getOd();
		odField.clear();
		odField.sendKeys(datumOd);
	}

	public WebElement getDo() {
		List<WebElement> dates = driver.findElements
				(By.xpath("//input [@ng-mode='data.date']"));
		return dates.get(5);
	}
	
	public void setDo(String datumDo) {
		WebElement doField = getDo();
		doField.clear();
		doField.sendKeys(datumDo);
	}
	
}
