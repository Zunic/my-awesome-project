package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InstitucijaIstrazivaciPage {
	private WebDriver driver;

	public InstitucijaIstrazivaciPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement angazujOsobu() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()=' Angažuj osobu']")));
	}
	
	public WebElement istrazivaciTable() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//table]")));
	}
	
	public WebElement getIstrazivacRowByName(String name) {
		return istrazivaciTable().findElement
				(By.xpath("//td[contains(text(), 'name')]"));
	}
	
	public WebElement deleteIstrazivacByName(String name) {
		return getIstrazivacRowByName(name).findElement(By.xpath("//button"));
	}
	
	public WebElement prviBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Prvi']")));
	}

	public WebElement prethodniBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Prethodni']")));
	}

	public WebElement sledeciBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Sledeći']")));
	}

	public WebElement poslednjiBtn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Poslednji']")));
	}
	
	public String nemaIstrazivacaMessage() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button[text()='Nema angažovanih osoba!']"))).getText();
	}

}
