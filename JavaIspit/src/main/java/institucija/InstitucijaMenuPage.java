package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InstitucijaMenuPage {
	private WebDriver driver;

	public InstitucijaMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement osnovniPodaci() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[1]/a")));
	}
	
	public WebElement podaciZaRegistar() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[2]/a")));
	}
	
	public WebElement podaciZaProjekte() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[3]/a")));
	}

	public WebElement istrazivaci() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[4]/a")));
	}

	public WebElement dodajIstrazivaca() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@ui-sref='addPerson']")));
	}	

}
