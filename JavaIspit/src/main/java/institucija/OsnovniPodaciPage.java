package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OsnovniPodaciPage {
	private WebDriver driver;

	public OsnovniPodaciPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNazivInstitucije() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='name']")));
	}
	
	public void setNazivInstitucije(String nazivInstitucije) {
		WebElement nazivInstitucijeField = getNazivInstitucije();
		nazivInstitucijeField.clear();
		nazivInstitucijeField.sendKeys(nazivInstitucije);
	}
	
	public WebElement getnazivInstitucijeNaEngleskom() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='eng_name']")));
	}
	
	public void setNazivInstitucijeNaEngleskom(String nazivInstitucijeNaEngleskom) {
		WebElement nazivInstitucijeNaEngleskomField = getnazivInstitucijeNaEngleskom();
		nazivInstitucijeNaEngleskomField.clear();
		nazivInstitucijeNaEngleskomField.sendKeys(nazivInstitucijeNaEngleskom);
	}

	public WebElement getDrzava() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='state']")));
	}

	public WebElement dodajNovuDrzavu() {
		getDrzava().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='-1']")));
	}
	
	public WebElement odaberiDrzavu(String drzava) {
		getDrzava().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + drzava + "']")));
	}

	public WebElement getNazivNoveDrzave() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='stateDescription']")));
	}
	
	public void setNazivNoveDrzave(String nazivNoveDrzave) {
		WebElement nazivNoveDrzaveField = getNazivNoveDrzave();
		nazivNoveDrzaveField.clear();
		nazivNoveDrzaveField.sendKeys(nazivNoveDrzave);
	}

	public WebElement getOpisNoveDrzave() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='name']")));
	}
	
	public void setOpisNoveDrzave(String opisNoveDrzave) {
		WebElement opisNoveDrzaveField = getOpisNoveDrzave();
		opisNoveDrzaveField.clear();
		opisNoveDrzaveField.sendKeys(opisNoveDrzave);
	}
	
	public WebElement sacuvajNovuDrzavu() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@type='submit']")));
	}

	public WebElement getMesto() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='place']")));
	}
	
	public void setMesto(String mesto) {
		WebElement mestoField = getMesto();
		mestoField.clear();
		mestoField.sendKeys(mesto);
	}

	public WebElement getOpstina() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='townShipText']")));
	}
	
	public void setOpstina(String opstina) {
		WebElement opstinaField = getOpstina();
		opstinaField.clear();
		opstinaField.sendKeys(opstina);
	}
	
	public WebElement getAdresa() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='address']")));
	}
	
	public void setAdresa(String adresa) {
		WebElement adresaField = getAdresa();
		adresaField.clear();
		adresaField.sendKeys(adresa);
	}
	
	public WebElement getWebAdresa() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='uri']")));
	}
	
	public void setWebAdresa(String webAdresa) {
		WebElement webAdresaField = getWebAdresa();
		webAdresaField.clear();
		webAdresaField.sendKeys(webAdresa);
	}

	public WebElement getEmail() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='email']")));
	}
	
	public void setEmail(String email) {
		WebElement emailField = getEmail();
		emailField.clear();
		emailField.sendKeys(email);
	}
	
	public WebElement getTelefon() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='phone']")));
	}
	
	public void setTelefon(String telefon) {
		WebElement telefonField = getTelefon();
		telefonField.clear();
		telefonField.sendKeys(telefon);
	}

	public WebElement getSkraceniNaziv() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='acro']")));
	}
	
	public void setSkraceniNaziv(String skraceniNaziv) {
		WebElement skraceniNazivField = getSkraceniNaziv();
		skraceniNazivField.clear();
		skraceniNazivField.sendKeys(skraceniNaziv);
	}
	
}
