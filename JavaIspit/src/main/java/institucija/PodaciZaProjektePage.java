package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PodaciZaProjektePage {
	private WebDriver driver;

	public PodaciZaProjektePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBrojRacuna() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='account']")));
	}
	
	public void setBrojRacuna(String brojRacuna) {
		WebElement brojRacunaField = getBrojRacuna();
		brojRacunaField.clear();
		brojRacunaField.sendKeys(brojRacuna);
	}

	public WebElement getIdUMinistarstvu() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='mntrID']")));
	}

	public WebElement getIdNaMedjNivou() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='account']")));
	}
	
	public void setIdNaMedjNivou(String idNaMedjNivou) {
		WebElement idNaMedjNivouField = getIdNaMedjNivou();
		idNaMedjNivouField.clear();
		idNaMedjNivouField.sendKeys(idNaMedjNivou);
	}

	public WebElement getStatusInstitucije() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='institutionStatus']")));
	}
	
	public void setStatusInstitucije(String statusInstitucije) {
		WebElement statusInstitucijeField = getStatusInstitucije();
		statusInstitucijeField.clear();
		statusInstitucijeField.sendKeys(statusInstitucije);
	}

	public WebElement getOblastIstrazivanja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("s2id_autogen37")));
	}
	
	public void setOblastIstrazivanja(String oblastIstrazivanja) {
		WebElement oblastIstrazivanjaField = getOblastIstrazivanja();
		oblastIstrazivanjaField.clear();
		oblastIstrazivanjaField.sendKeys(oblastIstrazivanja);
	}

	
}
