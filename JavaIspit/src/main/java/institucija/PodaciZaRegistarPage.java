package institucija;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PodaciZaRegistarPage {
	private WebDriver driver;

	public PodaciZaRegistarPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getPib() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='pib']")));
	}
	
	public void setPib(String pib) {
		WebElement pibField = getPib();
		pibField.clear();
		pibField.sendKeys(pib);
	}

	public WebElement getMaticniBroj() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='maticniBroj']")));
	}
	
	public void setMaticniBroj(String maticniBroj) {
		WebElement maticniBrojField = getMaticniBroj();
		maticniBrojField.clear();
		maticniBrojField.sendKeys(maticniBroj);
	}

	public WebElement getBrojPoslednjeAkreditacije() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='accreditationNumber']")));
	}
	
	public void setBrojPoslednjeAkreditacije(String brojPoslednjeAkreditacije) {
		WebElement brojPoslednjeAkreditacijeField = getBrojPoslednjeAkreditacije();
		brojPoslednjeAkreditacijeField.clear();
		brojPoslednjeAkreditacijeField.sendKeys(brojPoslednjeAkreditacije);
	}

	public WebElement getDatumPoslednjeAkreditacije() {
		List<WebElement> dates = driver.findElements
				(By.xpath("//input [@ng-model='data.date']"));
		return dates.get(0);
	}
	
	public void setDatumPoslednjeAkreditacije(String datumPoslednjeAkreditacije) {
		WebElement datumPoslednjeAkreditacijeField = getDatumPoslednjeAkreditacije();
		datumPoslednjeAkreditacijeField.clear();
		datumPoslednjeAkreditacijeField.sendKeys(datumPoslednjeAkreditacije);
	}

	public WebElement getNazivIzAkreditacije() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='accreditationNote']")));
	}
	
	public void setNazivIzAkreditacije(String nazivIzAkreditacije) {
		WebElement nazivIzAkreditacijeField = getNazivIzAkreditacije();
		nazivIzAkreditacijeField.clear();
		nazivIzAkreditacijeField.sendKeys(nazivIzAkreditacije);
	}

	public WebElement getNapomenaORegistru() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='note']")));
	}
	
	public void setNapomenaORegistru(String napomenaORegistru) {
		WebElement napomenaORegistruField = getNapomenaORegistru();
		napomenaORegistruField.clear();
		napomenaORegistruField.sendKeys(napomenaORegistru);
	}

	public WebElement getVrstaInstitucije() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//select [@ng-model='data.type']")));
	}
	
	public void setVrstaInstitucije(String vrstaInstitucije) {
		WebElement vrstaInstitucijeField = getVrstaInstitucije();
		vrstaInstitucijeField.clear();
		vrstaInstitucijeField.sendKeys(vrstaInstitucije);
	}

	public WebElement getOsnivac() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='founder']")));
	}
	
	public void getOsnivac(String osnivac) {
		WebElement osnivacField = getOsnivac();
		osnivacField.clear();
		osnivacField.sendKeys(osnivac);
	}

	public WebElement getDatumOsnivanja() {
		List<WebElement> dates = driver.findElements
				(By.xpath("//input [@ng-model='data.date']"));
		return dates.get(1);
	}
	
	public void setDatumOsnivanja(String datumOsnivanja) {
		WebElement datumOsnivanjaField = getDatumOsnivanja();
		datumOsnivanjaField.clear();
		datumOsnivanjaField.sendKeys(datumOsnivanja);
	}

	public WebElement getBrojResenja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='rescriptNumber']")));
	}
	
	public void setBrojResenja(String brojResenja) {
		WebElement brojResenjaField = getBrojResenja();
		brojResenjaField.clear();
		brojResenjaField.sendKeys(brojResenja);
	}
	
	public WebElement getVlasnickaStruktura() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@ng-model='ownershipStructure']")));
	}
	
	public WebElement odaberiVlasnickuStrukturu(String vlasnickaStruktura) {
		getVlasnickaStruktura().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + vlasnickaStruktura + "']")));
	}


}
