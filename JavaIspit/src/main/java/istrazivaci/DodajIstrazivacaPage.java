package istrazivaci;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DodajIstrazivacaPage {
	private WebDriver driver;

	public DodajIstrazivacaPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getLicniPodaci() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[1]")));
	}
	
	public WebElement getPodaciZaRegistar() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[2]")));
	}

	public WebElement getPodaciZaProjekte() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//ul [@class='nav nav-tabs']/li[3]")));
	}

	public WebElement getIme() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='firstNameText']")));
	}
	
	public void setIme(String ime) {
		WebElement imeField = getIme();
		imeField.clear();
		imeField.sendKeys(ime);
	}

	public WebElement getPrezime() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("personSearchLastNameT")));
	}
	
	public void setPrezime(String prezime) {
		WebElement prezimeField = getPrezime();
		prezimeField.clear();
		prezimeField.sendKeys(prezime);
	}

	public WebElement getImeJednogRoditelja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='middleName']")));
	}
	
	public void setImeJednogRoditelja(String imeJednogRoditelja) {
		WebElement imeJednogRoditeljaField = getImeJednogRoditelja();
		imeJednogRoditeljaField.clear();
		imeJednogRoditeljaField.sendKeys(imeJednogRoditelja);
	}

	public WebElement getTitulaIstrazivaca() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='personTitle']")));
	}
	
	public WebElement odaberiTituluIstrazivaca(String titulaIstrazivaca) {
		getTitulaIstrazivaca().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + titulaIstrazivaca + "']")));
	}

	public WebElement getDatumRodjenja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@ng-model='data.date']")));
	}
	
	public void setDatumRodjenja(String datumRodjenja) {
		WebElement datumRodjenjaField = getDatumRodjenja();
		datumRodjenjaField.clear();
		datumRodjenjaField.sendKeys(datumRodjenja);
	}

	public WebElement getDrzavaRodjenja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='state']")));
	}
	
	public void setDrzavaRodjenja(String drzavaRodjenja) {
		WebElement drzavaRodjenjaField = getDrzavaRodjenja();
		drzavaRodjenjaField.clear();
		drzavaRodjenjaField.sendKeys(drzavaRodjenja);
	}

	public WebElement getMestoRodjenja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='placeOfBirth']")));
	}
	
	public void setMestoRodjenja(String mestoRodjenja) {
		WebElement mestoRodjenjaField = getMestoRodjenja();
		mestoRodjenjaField.clear();
		mestoRodjenjaField.sendKeys(mestoRodjenja);
	}
	
	public WebElement getOpstinaRodjenja() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='townShipOfBirth']")));
	}
	
	public void setOpstinaRodjenja(String opstinaRodjenja) {
		WebElement opstinaRodjenjaField = getOpstinaRodjenja();
		opstinaRodjenjaField.clear();
		opstinaRodjenjaField.sendKeys(opstinaRodjenja);
	}
	
	public WebElement getDrzavaBoravista() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='stateOfResidence']")));
	}
	
	public void setDrzavaBoravista(String drzavaBoravista) {
		WebElement drzavaBoravistaField = getDrzavaBoravista();
		drzavaBoravistaField.clear();
		drzavaBoravistaField.sendKeys(drzavaBoravista);
	}

	public WebElement getMestoBoravista() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='city']")));
	}
	
	public void setMestoBoravista(String mestoBoravista) {
		WebElement mestoBoravistaField = getMestoBoravista();
		mestoBoravistaField.clear();
		mestoBoravistaField.sendKeys(mestoBoravista);
	}

	public WebElement getOpstinaBoravista() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='townShipOfResidence']")));
	}
	
	public void gstOpstinaBoravista(String opstinaBoravista) {
		WebElement opstinaBoravistaField = getOpstinaBoravista();
		opstinaBoravistaField.clear();
		opstinaBoravistaField.sendKeys(opstinaBoravista);
	}

	public WebElement getAdresa() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='address']")));
	}
	
	public void setAdresa(String adresa) {
		WebElement adresaField = getAdresa();
		adresaField.clear();
		adresaField.sendKeys(adresa);
	}

	public WebElement getPol() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='personTitle']")));
	}
	
	public WebElement odaberiPol(String gender) {
		getPol().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + gender + "']")));
	}

	public WebElement getJmbg() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='jmbg']")));
	}
	
	public void setJmbg(String jmbg) {
		WebElement jmbgField = getJmbg();
		jmbgField.clear();
		jmbgField.sendKeys(jmbg);
	}

	public WebElement getEmail() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='email']")));
	}
	
	public void setEmail(String email) {
		WebElement emailField = getEmail();
		emailField.clear();
		emailField.sendKeys(email);
	}

	public WebElement getTelefon() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='phones']")));
	}
	
	public void setTelefon(String telefon) {
		WebElement telefonField = getTelefon();
		telefonField.clear();
		telefonField.sendKeys(telefon);
	}

	public WebElement getLicnaWebAdresa() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='uri']")));
	}
	
	public void setLicnaWebAdresa(String licnaWebAdresa) {
		WebElement licnaWebAdresaField = getLicnaWebAdresa();
		licnaWebAdresaField.clear();
		licnaWebAdresaField.sendKeys(licnaWebAdresa);
	}
	
	public WebElement getStatusIstrazivaca() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//input [@name='personStatus']")));
	}
	
	public WebElement odaberiStatusIstrazivaca(String status) {
		getStatusIstrazivaca().click();
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//option [@value='" + status + "']")));
	}


}
