package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.webdriven.commands.SelectPopUp;

public class CreateOrEditIspitnaPrijava {
	private WebDriver driver;
	private ModalWindowPage modalWindowPage;

	public CreateOrEditIspitnaPrijava(WebDriver driver) {
		
		this.driver = driver;
		modalWindowPage = new ModalWindowPage(driver);
	}
	
	public WebElement getTeorija() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_teorija")));
	}
	
	public void setTeorija(String teorija) {
		WebElement teorijaField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_teorija")));
		teorijaField.clear();
		teorijaField.sendKeys(teorija);
	}
	
	public WebElement getZadaci() {
		return driver.findElement(By.id("field_zadaci"));
	}
	
	public void setZadaci(String zadaci) {
		WebElement zadaciField = driver.findElement(By.id("field_zadaci"));
		zadaciField.clear();
		zadaciField.sendKeys(zadaci);
	}

	public WebElement getIspitniRok() {
		return driver.findElement(By.id("field_ispitniRok"));
	}
	
	public void setIspitniRok(String ispitniRok) {
		WebElement ispitniRokField = driver.findElement(By.id("field_ispitniRok"));
		ispitniRokField.sendKeys(ispitniRok);
	}
	
	public WebElement getStudent() {
		return driver.findElement(By.id("field_student"));
	}
	
	public void setStudent(String student) {
		WebElement studentField = driver.findElement(By.id("field_student"));
		studentField.sendKeys(student);
	}
	public WebElement getPredmet() {
		return driver.findElement(By.id("field_predmet"));
	}
	
	public void setPredmet(String predmet) {
		WebElement predmetField = driver.findElement(By.id("field_predmet"));
		predmetField.sendKeys(predmet);
	}

	public void createOrEditIspitnaPrijava(String teorija, String zadaci, String ispitniRok, String student, String predmet) {
		setTeorija(teorija);
		setZadaci(zadaci);
		setIspitniRok(ispitniRok);
		setStudent(student);
		setPredmet(predmet);
		modalWindowPage.getSaveButton().click();
	}
}
