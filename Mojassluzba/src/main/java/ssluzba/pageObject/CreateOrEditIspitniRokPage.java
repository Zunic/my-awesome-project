package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateOrEditIspitniRokPage {
	private WebDriver driver;
	private ModalWindowPage modalWindowPage;

	public CreateOrEditIspitniRokPage(WebDriver driver) {
		
		this.driver = driver;
		modalWindowPage = new ModalWindowPage(driver);
	}

	public WebElement getNaziv() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_naziv")));
	}
	
	public void setNaziv(String naziv) {
		WebElement nazivField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_naziv")));
		nazivField.clear();
		nazivField.sendKeys(naziv);
	}
	
	public WebElement getPocetak() {
		return driver.findElement(By.id("field_pocetak"));
	}
	
	public void setPocetak(String pocetak) {
		WebElement pocetakField = driver.findElement(By.id("field_pocetak"));
		pocetakField.clear();
		pocetakField.sendKeys(pocetak);
	}

	public WebElement getKraj() {
		return driver.findElement(By.id("field_kraj"));
	}
	
	public void setkraj(String kraj) {
		WebElement krajField = driver.findElement(By.id("field_kraj"));
		krajField.clear();
		krajField.sendKeys(kraj);
	}

	public void createOrEditIspitniRok(String naziv, String pocetak, String kraj) {
		setNaziv(naziv);
		setPocetak(pocetak);
		setkraj(kraj);
		modalWindowPage.getSaveButton().click();
	}
}
