package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateOrEditNastavnikPage {
	private WebDriver driver;
	private ModalWindowPage modalWindowPage;

	public CreateOrEditNastavnikPage(WebDriver driver) {
		
		this.driver = driver;
		modalWindowPage = new ModalWindowPage(driver);
	}
	
	public WebElement getIme() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_ime")));
	}
	
	public void setIme(String ime) {
		WebElement imeField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_ime")));
		imeField.clear();
		imeField.sendKeys(ime);
	}
	
	public WebElement getPrezime() {
		return driver.findElement(By.id("field_prezime"));
	}
	
	public void setPrezime(String prezime) {
		WebElement prezimeField = driver.findElement(By.id("field_prezime"));
		prezimeField.clear();
		prezimeField.sendKeys(prezime);
	}

	public WebElement getZvanje() {
		return driver.findElement(By.id("field_prezime"));
	}
	
	public void setZvanje(String zvanje) {
		WebElement zvanjeField = driver.findElement(By.id("field_zvanje"));
		zvanjeField.clear();
		zvanjeField.sendKeys(zvanje);
	}

	public void createOrEditNastavnnik(String ime, String prezime, String zvanje) {
		setIme(ime);
		setPrezime(prezime);
		setZvanje(zvanje);
		modalWindowPage.getSaveButton().click();
	}
}
