package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateOrEditPredmetPage {
	private WebDriver driver;

	public CreateOrEditPredmetPage(WebDriver driver) {
		
		this.driver = driver;
	}
	
	public WebElement getNaziv() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_naziv")));
	}
	
	public void setNaziv(String naziv) {
		WebElement nazivField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_naziv")));
		nazivField.clear();
		nazivField.sendKeys(naziv);
	}
	
	public WebElement getStudentiField() {
		WebElement studentifield = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_studenti")));
		return studentifield;
	}
	
	public String getStudenti() {
		return getStudentiField().getText();
	}
	
	public WebElement getNastavniciField() {
		WebElement nastavnicifield = driver.findElement(By.id("field_nastavnici"));
		return nastavnicifield;
	}
	
	public String getNastavnici() {
		return getNastavniciField().getText();
	}
	
	public void selectStudent(String ime, String prezime) {
		getStudentiField().findElement(By.xpath("//option [text()='" + ime + " " + prezime + "']")).click();;
	}
	
	public void selectNastavnik(String ime, String prezime) {
		getNastavniciField().findElement(By.xpath("//option [text()='" + ime + " " + prezime + "']")).click();
	}

}
