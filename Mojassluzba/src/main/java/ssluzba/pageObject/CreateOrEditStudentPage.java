package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateOrEditStudentPage {
	private WebDriver driver;
	private ModalWindowPage modalWindowPage;

	public CreateOrEditStudentPage(WebDriver driver) {
		
		this.driver = driver;
		modalWindowPage = new ModalWindowPage(driver);
	}
	
	public WebElement getIndeks() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_indeks")));
	}
	
	public void setIndeks(String indeks) {
		WebElement indeksField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("field_indeks")));
		indeksField.clear();
		indeksField.sendKeys(indeks);
	}
	
	public WebElement getIme() {
		return driver.findElement(By.id("field_ime"));
	}
	
	public void setIme(String ime) {
		WebElement imeField = driver.findElement(By.id("field_ime"));
		imeField.clear();
		imeField.sendKeys(ime);
	}

	public WebElement getPrezime() {
		return driver.findElement(By.id("field_prezime"));
	}
	
	public void setPrezime(String prezime) {
		WebElement prezimeField = driver.findElement(By.id("field_prezime"));
		prezimeField.clear();
		prezimeField.sendKeys(prezime);
	}
	
	public WebElement getGrad() {
		return driver.findElement(By.id("field_grad"));
	}
	
	public void setGrad(String grad) {
		WebElement gradField = driver.findElement(By.id("field_grad"));
		gradField.clear();
		gradField.sendKeys(grad);
	}
			
	public void createOrEditStudent(String indeks, String ime, String prezime, String grad) {
		setIndeks(indeks);
		setIme(ime);
		setPrezime(prezime);
		setGrad(grad);
		modalWindowPage.getSaveButton().click();
	}
}
