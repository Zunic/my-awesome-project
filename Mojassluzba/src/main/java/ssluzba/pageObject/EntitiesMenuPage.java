package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EntitiesMenuPage {
	private WebDriver driver;

	public EntitiesMenuPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getEntitiesMenu() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//span [@translate='global.menu.entities.main']")));
	}
	
	public WebElement getIspitniRokovi() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@ui-sref='ispitniRokovi']")));
	}
	
	public WebElement getNastavnici() {
		return driver.findElement(By.xpath("//a [@ui-sref='nastavnici']"));
	}
	
	public WebElement getStudenti() {
		return driver.findElement(By.xpath("//a [@ui-sref='studenti']"));
	}
	
	public WebElement getPredmeti() {
		return driver.findElement(By.xpath("//a [@ui-sref='predmeti']"));
	}
	
	public WebElement getIspitnePrijave() {
		return driver.findElement(By.xpath("//a [@ui-sref='ispitnePrijave']"));
	}
	
}
