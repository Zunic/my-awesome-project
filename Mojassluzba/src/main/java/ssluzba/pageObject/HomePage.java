package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	public WebDriver driver;
		
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public String getHomePageMessage() {
		return 	(new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//div [@translate=\"main.logged.message\"]"))).getText();
	}
}