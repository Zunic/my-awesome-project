package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IspitnePrijavePage {

	private WebDriver driver;

	public IspitnePrijavePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCreateIspitnaPrijava() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@ui-sref='ispitnePrijave.new']")));
	}
	
	public WebElement getIspitnaPrijavaRow(String ime, String predmet) {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//tr [contains(text()='" + ime + " " + predmet + "')]/../..")));
	}
	
	public void getDeleteButton(String ime, String predmet) {
		getIspitnaPrijavaRow(ime, predmet).findElement(By.className("btn-danger")).click();
	}

	public void getEditButton(String ime, String predmet) {
		getIspitnaPrijavaRow(ime, predmet).findElement(By.className("btn-primary")).click();
	}

	public void getViewButton(String ime, String predmet) {
		getIspitnaPrijavaRow(ime, predmet).findElement(By.className("btn-info")).click();
	}

}
