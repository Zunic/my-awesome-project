package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IspitniRokoviPage {
	private WebDriver driver;

	public IspitniRokoviPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCreateIspitniRok() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@ui-sref='ispitniRokovi.new']")));
	}
	
	public WebElement getIspitniRokRow(String naziv) {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [text()='" + naziv + "']/../..")));
	}
	
	public void getDeleteButton(String indeks) {
		getIspitniRokRow(indeks).findElement(By.className("btn-danger")).click();
	}

	public void getEditButton(String indeks) {
		getIspitniRokRow(indeks).findElement(By.className("btn-primary")).click();
	}

	public void getViewButton(String indeks) {
		getIspitniRokRow(indeks).findElement(By.className("btn-info")).click();
	}


	
}
