package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getUsername() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("username")));
	}
	public void setUsername(String username) {
		WebElement usernameField = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("username")));
		usernameField.clear();
		usernameField.sendKeys(username);
	}
	
	public WebElement getPassword() {
		return driver.findElement(By.id("password"));
	}
	public void setPassword(String password) {
		WebElement passwordField = driver.findElement(By.id("password"));
		passwordField.clear();
		passwordField.sendKeys(password);
	}
	
	public WebElement getSubmitButton() {
		return driver.findElement(By.xpath("//button [@type='submit']"));
	}
	
	public void navigateToPage(){
		driver.navigate().to("localhost:9000/#/login");
	}
	
	public void login(String username, String password) {
		setUsername(username);
		setPassword(password);
		getSubmitButton().click();
	}


}
