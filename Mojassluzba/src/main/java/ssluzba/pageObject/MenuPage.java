package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuPage {

	private WebDriver driver;

	public MenuPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getAccountMenu() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("account-menu")));
	}
	
	public WebElement getSignIn() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [@ui-sref='login']")));
	}
	
	public WebElement getRegister() {
		return driver.findElement(By.xpath("//a [@ui-sref='register']"));
	}
	
	public WebElement getLogout() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.id("logout")));
	}
}
