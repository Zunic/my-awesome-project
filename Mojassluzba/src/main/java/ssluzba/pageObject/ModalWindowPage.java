package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ModalWindowPage {

	private WebDriver driver;

	public ModalWindowPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCancelButton() {
		WebElement modalFooter = driver.findElement(By.xpath("//div [@class='modal-footer']"));
		WebElement cancelBtn = modalFooter.findElement(By.xpath("//button [@class='btn-default']"));
		return cancelBtn;
	}

	public WebElement getSaveButton() {
		WebElement modalFooter = driver.findElement(By.xpath("//div [@class='modal-footer']"));
		WebElement saveBtn = modalFooter.findElement(By.xpath("/html/body/div[5]/div/div/form/div[3]/button[2]"));
		return saveBtn;
	}
	
	public WebElement getDeleteButton() {
		WebElement modalFooter = driver.findElement(By.xpath("//div [@class='modal-footer']"));
		WebElement deleteBtn = modalFooter.findElement(By.className("btn-danger"));
		return deleteBtn;
	}

}
