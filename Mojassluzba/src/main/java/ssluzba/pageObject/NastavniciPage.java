package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NastavniciPage {
	private WebDriver driver;

	public NastavniciPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCreateNastavnik() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@ui-sref='nastavnici.new']")));
	}
	
	public WebElement getNastavnikRow(String prezime) {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//td [text()='" + prezime + "']/../..")));
	}
	
	public void getDeleteButton(String indeks) {
		getNastavnikRow(indeks).findElement(By.className("btn-danger")).click();
	}

	public void getEditButton(String indeks) {
		getNastavnikRow(indeks).findElement(By.className("btn-primary")).click();
	}

	public void getViewButton(String indeks) {
		getNastavnikRow(indeks).findElement(By.className("btn-info")).click();
	}

}
