package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PredmetiPage {
	private WebDriver driver;

	public PredmetiPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCreatePredmet() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@class='btn btn-primary']")));
	}
	
	public WebElement getPredmetRow(String nazivPredmeta) {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [text()='" + nazivPredmeta + "']/../..")));
	}
	
	public void getDeleteButton(String indeks) {
		getPredmetRow(indeks).findElement(By.className("btn-danger")).click();
	}

	public void getEditButton(String indeks) {
		getPredmetRow(indeks).findElement(By.className("btn-primary")).click();
	}

	public void getViewButton(String indeks) {
		getPredmetRow(indeks).findElement(By.className("btn-info")).click();
	}

	
}
