package ssluzba.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentPage {
	private WebDriver driver;

	public StudentPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getCreateStudent() {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//button [@ui-sref='studenti.new']")));
	}
	
	public WebElement getStudentRow(String indeks) {
		return (new WebDriverWait(driver,10))
				.until(ExpectedConditions.presenceOfElementLocated
						(By.xpath("//a [text()='" + indeks + "']/../..")));
	}
	
	public void getDeleteButton(String indeks) {
		getStudentRow(indeks).findElement(By.className("btn-danger")).click();
	}

	public void getEditButton(String indeks) {
		getStudentRow(indeks).findElement(By.className("btn-primary")).click();
	}

	public void getViewButton(String indeks) {
		getStudentRow(indeks).findElement(By.className("btn-info")).click();
	}
	

}
