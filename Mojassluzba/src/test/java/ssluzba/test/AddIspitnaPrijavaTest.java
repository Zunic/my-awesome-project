package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditIspitnaPrijava;
import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.IspitnePrijavePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.StudentPage;

public class AddIspitnaPrijavaTest {
	
	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private IspitnePrijavePage ispitnePrijavePage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditIspitnaPrijava createOrEditIspitnaPrijava;
	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		ispitnePrijavePage = new IspitnePrijavePage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditIspitnaPrijava = new CreateOrEditIspitnaPrijava(driver);
	}
	
	@Test
	public void addIspitnaPrijava() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getIspitnePrijave().click();
		
		ispitnePrijavePage.getCreateIspitnaPrijava().click();
		createOrEditIspitnaPrijava.createOrEditIspitnaPrijava(null, null, "Aprilski", "E1234 Marko Markovic", "Matematika");
		
		WebElement newIspitnaPrijava = ispitnePrijavePage.getIspitnaPrijavaRow("E1234 Marko Markovic", "Matematika");
//		assertEquals(true, newIspitnaPrijava.isDisplayed());
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();		

	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}


}
