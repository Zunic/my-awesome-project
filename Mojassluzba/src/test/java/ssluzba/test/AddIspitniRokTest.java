package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditIspitniRokPage;
import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.IspitniRokoviPage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.StudentPage;

public class AddIspitniRokTest {
	
	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private IspitniRokoviPage ispitniRokovi;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditIspitniRokPage createOrEditIspitniRok;
	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {

		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		ispitniRokovi = new IspitniRokoviPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditIspitniRok = new CreateOrEditIspitniRokPage(driver);
	}
	
	@Test
	public void addIspitniRok() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getIspitniRokovi().click();
		
		ispitniRokovi.getCreateIspitniRok().click();
		createOrEditIspitniRok.createOrEditIspitniRok("Aprilski", "2016-04-15", "2016-04-22");
		
		assertEquals(true, ispitniRokovi.getIspitniRokRow("Aprilski").isDisplayed());
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();		

	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}


}
