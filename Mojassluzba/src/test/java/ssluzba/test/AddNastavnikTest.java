package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditNastavnikPage;
import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.NastavniciPage;
import ssluzba.pageObject.StudentPage;

public class AddNastavnikTest {

	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private NastavniciPage nastavniciPage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditNastavnikPage createOrEditNastavnikPage;

	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		nastavniciPage = new NastavniciPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditNastavnikPage = new CreateOrEditNastavnikPage(driver);
	}

	@Test
	public void addNastavnik() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getNastavnici().click();
		
		nastavniciPage.getCreateNastavnik().click();
		createOrEditNastavnikPage.createOrEditNastavnnik("Milan", "Markovic", "Profesor");
		
		WebElement newNastavnik = nastavniciPage.getNastavnikRow("Markovic");
		assertEquals(true, newNastavnik.getText().contains("Milan Markovic Profesor"));
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();		
	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}

}
