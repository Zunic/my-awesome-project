package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditPredmetPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.ModalWindowPage;
import ssluzba.pageObject.PredmetiPage;

public class AddPredmetTest {

	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private PredmetiPage predmetiPage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditPredmetPage createOrEditPredmetPage;
	private ModalWindowPage ModalWindowPage;
	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		predmetiPage = new PredmetiPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditPredmetPage = new CreateOrEditPredmetPage(driver);
		ModalWindowPage = new ModalWindowPage(driver);
	}

	@Test
	public void addPredmet() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getPredmeti().click();
		
		predmetiPage.getCreatePredmet().click();
		createOrEditPredmetPage.setNaziv("Matematika");
		createOrEditPredmetPage.selectStudent("Marko", "Markovic");
		createOrEditPredmetPage.selectNastavnik("Milan", "Markovic");
		ModalWindowPage.getSaveButton().click();
		
//		assertEquals(true, predmetiPage.getPredmetRow("Matematika").getText().contains("Matematika Marko Markovic Milan Markovic"));
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();		

	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}

}
