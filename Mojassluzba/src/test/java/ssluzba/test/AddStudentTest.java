package ssluzba.test;

import java.util.concurrent.TimeUnit;
import static org.testng.AssertJUnit.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.StudentPage;

public class AddStudentTest {
	
	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private StudentPage studentPage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditStudentPage createOrEditStudentPage;
	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		studentPage = new StudentPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditStudentPage = new CreateOrEditStudentPage(driver);
	}
	
	@Test
	public void addStudent() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
		studentPage.getCreateStudent().click();		
		createOrEditStudentPage.createOrEditStudent("E1234", "Marko", "Markovic", "Novi Sad");
				
		WebElement newStudent1 = studentPage.getStudentRow("E1234");		
		assertEquals(true, newStudent1.getText().contains("E1234 Marko Markovic Novi Sad"));
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
		studentPage.getCreateStudent().click();		
		createOrEditStudentPage.createOrEditStudent("E5652", "Nikola", "Nikolic", "Beograd");
				
		WebElement newStudent2 = studentPage.getStudentRow("E5652");		
		assertEquals(true, newStudent2.getText().contains("E5652 Nikola Nikolic Beograd"));
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();		
	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}
	
	
	
}
