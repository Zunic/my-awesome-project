package ssluzba.test;

import static org.testng.AssertJUnit.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.ModalWindowPage;
import ssluzba.pageObject.StudentPage;

public class DeleteStudentTest {

	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private StudentPage studentPage;
	private EntitiesMenuPage entitiesMenuPage;
	private ModalWindowPage modalWindowPage;

	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}

	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		studentPage = new StudentPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		modalWindowPage = new ModalWindowPage(driver);
	}

	@Test
	public void deleteStudent() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
//		WebElement newStudent1 = studentPage.getStudentRow("E1234");
//		studentPage.getDeleteButton("E1234");
//		modalWindowPage.getDeleteButton().click();;
//		
		
		WebElement newStudent2 = studentPage.getStudentRow("E5652");	
		studentPage.getDeleteButton("E5652");
		modalWindowPage.getDeleteButton().click();
		
		assertEquals(false, newStudent2.isDisplayed());

		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();

	}
	
	
		
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}
}
