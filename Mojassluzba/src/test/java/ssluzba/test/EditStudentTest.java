package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.StudentPage;

public class EditStudentTest {
	
	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private StudentPage studentPage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditStudentPage createOrEditStudentPage;

	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		studentPage = new StudentPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditStudentPage = new CreateOrEditStudentPage(driver);
	}

	@Test
	public void editStudent() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
		studentPage.getEditButton("E5652");
		createOrEditStudentPage.createOrEditStudent("E5652", "Nikola", "Nikolic", "Kraljevo");
		
		WebElement newStudent = studentPage.getStudentRow("E5652");		
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();
	}
	
	@AfterTest
	public void closeSelenium() {
		driver.close();
	}

}
