package ssluzba.test;

import static org.testng.AssertJUnit.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;

public class LoginTest {
	
	private WebDriver driver;
	public LoginPage loginPage;
	public HomePage homePage;
	public MenuPage menuPage;
	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
	}
	
	@BeforeTest
	public void setupPages() {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		menuPage = new MenuPage(driver);
	}
	@Test
	public void loginTest() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.getUsername().clear();
		loginPage.setUsername("admin");
		
		loginPage.getPassword().clear();
		loginPage.setPassword("admin");
		
		loginPage.getSubmitButton().click();
		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
	}
}
