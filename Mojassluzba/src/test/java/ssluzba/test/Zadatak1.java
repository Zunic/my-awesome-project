package ssluzba.test;

import static org.testng.AssertJUnit.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ssluzba.pageObject.CreateOrEditStudentPage;
import ssluzba.pageObject.EntitiesMenuPage;
import ssluzba.pageObject.HomePage;
import ssluzba.pageObject.LoginPage;
import ssluzba.pageObject.MenuPage;
import ssluzba.pageObject.ModalWindowPage;
import ssluzba.pageObject.StudentPage;

public class Zadatak1 {

	private WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private StudentPage studentPage;
	private EntitiesMenuPage entitiesMenuPage;
	private CreateOrEditStudentPage createOrEditStudentPage;
	private ModalWindowPage modalWindowPage;


	
	@BeforeSuite
	public void setupSelenium() {
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to("localhost:9000/#/");
		
		homePage = new HomePage(driver);
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		studentPage = new StudentPage(driver);
		entitiesMenuPage = new EntitiesMenuPage(driver);
		createOrEditStudentPage = new CreateOrEditStudentPage(driver);
		modalWindowPage = new ModalWindowPage(driver);
	}
	
	@Test
	public void addStudent() {
		
		menuPage.getAccountMenu().click();
		menuPage.getSignIn().click();
		
		loginPage.login("admin", "admin");		
		assertEquals("You are logged in as user \"admin\".", homePage.getHomePageMessage());
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
		studentPage.getCreateStudent().click();		
		createOrEditStudentPage.createOrEditStudent("E1234", "Marko", "Markovic", "Novi Sad");
				
		WebElement newStudent1 = studentPage.getStudentRow("E1234");		
		assertEquals(true, newStudent1.getText().contains("E1234 Marko Markovic Novi Sad"));
		
		entitiesMenuPage.getEntitiesMenu().click();
		entitiesMenuPage.getStudenti().click();
		
		studentPage.getCreateStudent().click();		
		createOrEditStudentPage.createOrEditStudent("E5652", "Nikola", "Nikolic", "Beograd");
				
		WebElement newStudent2 = studentPage.getStudentRow("E5652");		
		assertEquals(true, newStudent2.getText().contains("E5652 Nikola Nikolic Beograd"));
			
	}

	@Test(dependsOnMethods = "addStudent")
	public void editStudent() {
		
		
		studentPage.getEditButton("E5652");
		createOrEditStudentPage.createOrEditStudent("E5652", "Nikola", "Nikolic", "Kraljevo");
		
		WebElement newStudent = studentPage.getStudentRow("E5652");		
		assertEquals(true, newStudent.getText().contains("E5652 Nikola Nikolic Kraljevo"));
		

	}

	@Test(dependsOnMethods = "editStudent")
	public void deleteStudent() {
		
		
		WebElement newStudent1 = studentPage.getStudentRow("E1234");
		studentPage.getDeleteButton("E1234");
		modalWindowPage.getDeleteButton().click();
		
		
		WebElement newStudent2 = studentPage.getStudentRow("E5652");	
		studentPage.getDeleteButton("E5652");
		modalWindowPage.getDeleteButton().click();
		
		assertEquals(false, newStudent1.isDisplayed());
		assertEquals(false, newStudent2.isDisplayed());
					

	}
	
	@Test(dependsOnMethods = "deleteStudent")
	public void logOutTest() {
		
		menuPage.getAccountMenu().click();
		menuPage.getLogout().click();
		
	};


	@AfterSuite
	public void closeSelenium() {
		driver.close();
	}
		
	
}
