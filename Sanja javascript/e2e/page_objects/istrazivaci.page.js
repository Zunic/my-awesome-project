var IstrazivacRow = require('./istrazivaciRow.page.js');

var IstrazivaciPage = function() {}

IstrazivaciPage.prototype = Object.create({}, {

    dodajIstrazivacaBtn: {
        get: function() {
            return element(by.xpath("//a[@ui-sref='addPerson']"));
        }
    },

    pretrazivac: {
        get: function() {
            return element(by.model("value"));
        },
        set: function(value) {
            this.pretrazivac.clear();
            this.pretrazivac.sendKeys(value);
        }
    },

    migriraniPodaci: {
        get: function() {
            return element(by.name("migrated"));
        }
    },

    verifikovaniMigriraniPodaci: {
        get: function() {
            return element(by.name("changed"));
        }
    },

    istrazivaciTable: {
        get: function() {
            return element(by.xpath("//*[@id='page-content']//table"));
        }
    },

    tableRows: {
        get: function() {
            return element.all(by.tagName("tr"));
        }
    },
    
    istrazivacRowByIme:{
        value:function(imeString) {
            return this.istrazivaciTable.element(by.xpath('//*[contains(text(),"' + imeString + '")]/..'));

        }
    },

    editIstrazivacByPrezime:{
      value:function(prezimeString) {

        var edit = this.istrazivacRowByPrezime(prezimeString).element(by.xpath('//td[contains(text(),"' + prezimeString + '")]/..'));
        edit.click();
      }
    },

    sortByIme: {
        get: function() {
            return element(by.xpath("//span[contains(text(),'Ime')]"));
        }
    },

    sortByPrezime: {
        get: function() {
            return element(by.xpath("//span[contains(text(), 'Prezime')]"));
        }
    },

    sortByDatum: {
        get: function() {
            return element(by.xpath("//span[contains(text(), 'Datum')]"));
        }
    },

    prviBtn: {
        get: function() {
            return element(by.xpath("//a[contains(text(), 'Prvi')]"));
        }
    },

    prethodniBtn: {
        get: function() {
            return element(by.xpath("//a[contains(text(), 'Prethodni')]"));
        }
    },

    trenutniBtn: {
        get: function() {
            return element(by.xpath("//a[@ng-click='selectPage(page.number, $event)']"));
        }
    },

    sledeciBtn: {
        get: function() {
            return element(by.xpath("//a[contains(text(), 'Sledeći')]"));
        }
    },

    poslednjiBtn: {
        get: function() {
            return element(by.xpath("//a[contains(text(), 'Poslednji')]"));
        }
    }

});

module.exports = IstrazivaciPage;