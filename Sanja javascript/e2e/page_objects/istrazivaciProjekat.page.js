var IstrazivaciProjekatPage = function() {}

IstrazivaciProjekatPage.prototype = Object.create({}, {

    tipOsobe: {
        get: function() {
            return element(by.model("data.bibliography"));
        },
        set: function(value) {
            this.bibilografija.clear();
            this.bibilografija.sendKeys(value);
        }
    },

   

// --------------------- G R E S K E -----------------------
    tipOsobeError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Tip osobe *\"]//span");
        }
    },




 // -------- dugmici -------
    odustaniBtn: {
        get: function() {
            return element(by.xpath("//button [@ng-click='reset()']"));
        }
    },

    sacuvajBtn: {
        get: function() {
            return element(by.xpath("//button [@ng-click='addctrl.savePerson(Project)']"));
        }
    }

});

module.exports = IstrazivaciProjekatPage;
