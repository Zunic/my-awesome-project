var LoginPage = function() {}

LoginPage.prototype = Object.create({}, {
    
    loginText: {
        get: function() {
            return element(by.className("login-logo")).getText();
        }
    },

    // Username polje
    username: {
        get: function() {
            return element(by.model("login.user"));
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },
    
    // Password polje
    password: {
        get: function() {
            return element(by.model("login.password"));
        },
        set: function(value) {
            this.password.clear();
            this.password.sendKeys(value);
        }
    },

   odustaniBtn: {
        get: function() {
            return element(by.buttonText('Odustani'));
        }
    },

    prijaviSeBtn: {
        get: function() {
            return element(by.buttonText('Prijavi se'));
        }
    },

    zaboravljenaLozinkaBtn: {
        get: function() {
            return element(by.buttonText('Zaboravili ste lozinku?'));
        }
    },

    loginProblem: {
        get: function() {
            return element(by.repeater("error in errors track by $index")).getText();
        }
    }



});

module.exports = LoginPage;