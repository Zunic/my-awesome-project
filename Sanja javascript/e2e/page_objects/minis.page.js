var MinisPage = function() {}

MinisPage.prototype = Object.create({}, {

    menuMinis: {
        get: function() {
            return element.all(by.xpath('//ul[@class="nav nav-tabs"]/li'));
        }
    },

    osnovniPodaci: {
        get: function() {
            return element(by.xpath('//ul[@class="nav nav-tabs"]/li[1]'));
        }
    },

    podaciZaRegistar: {
        get: function() {
            return element(by.xpath("//ul[@class='nav nav-tabs']/li[2]"));
        }
    },

    podaciZaProjekte: {
        get: function() {
            return element(by.xpath('//ul[@class="nav nav-tabs"]/li[3]'));
        }
    },

    jezik: {
        get: function() {
            return element(by.className('caret'));
        }
    },

    srpskiLat: {
        get: function() {
            return element(by.linkText("Srpski"));
        }
    },

    srpskiCir: {
        get: function() {
            return element(by.linkText("Српски"));
        }
    },

    engleski: {
        get: function() {
            return element(by.linkText("English"));
        }
    },

    zastavica: {
        get: function() {
            return element(by.xpath("//a[@class='dropdown-toggle']"));
        }
    },

    odjava: {
        get: function() {
            return element(by.xpath("//span[@translate='LOGOUT']"));
        }
    },

    institucijeBtn: {
        get: function() {
            return element(by.xpath("//li[@title='Institucija']"));
        }
    },

    istrazivaciBtn: {
        get: function() {
            return element(by.xpath("//li[@title='Istraživači']"));
        }
    }
    

});

module.exports = MinisPage;
