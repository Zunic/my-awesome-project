var OsnovniPodaciPage = function() {}

OsnovniPodaciPage.prototype = Object.create({}, {


    cijelaForma: {
        get: function() {
            return element(by.xpath("//form[@name='Basic']"));
        }
    },



    nazivInstitucije: {
        get: function() {
            return element(by.model("data.name"));
        },
        set: function(value) {
            this.nazivInstitucije.clear();
            this.nazivInstitucije.sendKeys(value);
        }
    },

    
    nazivInstitucijeEng: {
        get: function() {
            return element(by.model("data.nameEn"));
        },
        set: function(value) {
            this.nazivInstitucijeEng.clear();
            this.nazivInstitucijeEng.sendKeys(value);
        }
    },

    drzava: {
        get: function() {
            return element(by.model("data.state"));
        },
        set: function(value) {
            this.drzava.sendKeys(value);
        }
    },


// unos nove drzave
    novaDrzava: {
        get: function() {
            return element(by.model('addctrl.state.name'));
        },
        set: function(value) {
         this.novaDrzava.clear();
         this.novaDrzava.sendKeys(value);
        }
    },

    opisNoveDrzave: {
        get: function() {
            return element(by.model('addctrl.state.description'));
        },
        set: function(value) {
         this.opisNoveDrzave.clear();
         this.opisNoveDrzave.sendKeys(value);
        }
    },

    tacnoBtn: {
        get: function() {
            return element(by.name('btnSave'));
        }
    },

    mesto: {
        get: function() {
            return element(by.xpath("//input[@name='place']"));
        },
        set: function(value) {
            this.mesto.clear();
            this.mesto.sendKeys(value);
        }
    },

    

    opstina: {
        get: function() {
            return element(by.xpath("//input[@name='townShipText']"));
        },
        set: function(value) {
            this.opstina.clear();
            this.opstina.sendKeys(value);
        }
    },


     ulicaIBroj: {
        get: function() {
            return element(by.model("data.address"));
        },
        set: function(value) {
            this.ulicaIBroj.clear();
            this.ulicaIBroj.sendKeys(value);
        }
    },

    

     webAdresa: {
        get: function() {
            return element(by.model("data.uri"));
        },
        set: function(value) {
            this.webAdresa.clear();
            this.webAdresa.sendKeys(value);
        }
    },

    

     elektronskaPosta: {
        get: function() {
            return element(by.model("data.email"));
        },
        set: function(value) {
            this.elektronskaPosta.clear();
            this.elektronskaPosta.sendKeys(value);
        }
    },

   

    telefon: {
        get: function() {
            return element(by.model("data.phone"));
        },
        set: function(value) {
            this.telefon.clear();
            this.telefon.sendKeys(value);
        }
    },

    

     skraceniNaziv: {
        get: function() {
            return element(by.model("data.acro"));
        },
        set: function(value) {
            this.skraceniNaziv.clear();
            this.skraceniNaziv.sendKeys(value);
        }
    },


// glupavi dropDown
    // institucijaDDklik: {
    //     get: function() {
    //         return element(by.xpath("//a[@class='select2-choice']"));
    //     }
    // },


    // sadKlikNaInstituciju: {
    //         value: function(ime) {
    //         return element(by.xpath("//option[text()=\"" + ime + "\"]")).click();
    //     }
    // },


    // provjeraSelekcije: {
    //     get: function() {
    //         return element(by.xpath("//option[text()='Neda Raspopovic']"));
    //     }
    // },


// -------------------------G R E S K E ------------------

    nazivInstitucijeError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Naziv institucije *\"]//span"));
        }
    },


    mestoError: {
        get : function() {
            return element(by.xpath("//span[contains(text(),'Morate uneti mesto.')]"));
        }
    },

    ulicaIBrojError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Ulica i broj *\"]//span"));
        }
    },

    webAdresaError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Veb adresa *\"]//span"));
        }
    },

     elektronskaPostaError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Elektronska pošta *\"]//span"));
        }
    },

    telefonError: {
        get : function() {
            return element(by.xpath("//div[@title=\"Telefon *\"]//span"));
        }
    },





// -------- dugmici -------
    odustaniBtn: {
        get: function() {
            return element(by.xpath("//button[@ng-click='reset(Basic)']"));
        }
    },

    sacuvajBtn: {
        get: function() {
            return element(by.xpath("//button[@ng-click='addctrl.saveInstitution(Basic)']"));
        }
    }


});

module.exports = OsnovniPodaciPage;