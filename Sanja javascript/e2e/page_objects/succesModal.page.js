var SuccesModalPage = function() {};

SuccesModalPage.prototype = Object.create({}, {

      succesModalAppears: {
        get: function() {
            return element(by.xpath('//div[@class="ui-pnotify-text"]'));
        }
    },

    pauseBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-pause"]'));
        }
    },

    playBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-play"]'));
        }
    },

    xBtn: {
        get: function() {
            return element(by.xpath('//span[@class="fa fa-times"]'));
        }
    }

});

module.exports = SuccesModalPage;