var LoginPage = require('../page_objects/logIn.page.js');
var OsnovniPodaciPage = require('../page_objects/osnovniPodaci.page.js');
var SuccesModalPage = require('../page_objects/succesModal.page.js');
var IstrazivaciPage = require('../page_objects/istrazivaci.page.js');
var MinisPage = require('../page_objects/minis.page.js');
var CreateIstrazivaciPage = require('../page_objects/createIstrazivaci.page.js');
var AngazujPage = require('../page_objects/angazuj.page.js');

describe('Login page:', function() {
  var loginPage;
  var osnovniPodaciPage;
  var succesModalPage;
  var istrazivaciPage;
  var minisPage;
  var createIstrazivaci;
  var angazujPage;

  beforeAll(function() {
    browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
    loginPage = new LoginPage();
    osnovniPodaciPage = new OsnovniPodaciPage();
    succesModalPage = new SuccesModalPage();
    istrazivaciPage = new IstrazivaciPage();
    minisPage = new MinisPage();
    createIstrazivaci = new CreateIstrazivaciPage();
    angazujPage = new AngazujPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://park.ftn.uns.ac.rs:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {

// cekanje da se pojavi login stranica
    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "11223344";
    loginPage.prijaviSeBtn.click();

     browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/admin-institution/';
      });
    }, 5000, 'Could not navigate to admin');


  });

  it('Istrazivaciii."', function() {

        expect(minisPage.istrazivaciBtn.isPresent()).toBe(true);
        expect(minisPage.istrazivaciBtn.isDisplayed()).toBe(true);
        expect(minisPage.istrazivaciBtn.isEnabled()).toBe(true);

//klik na link

       minisPage.istrazivaciBtn.click();

       browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                 return url === 'http://park.ftn.uns.ac.rs:8080/#/persons';
      });
    }, 5000, 'Istrazivaci stranica ne može da se učita');


    expect(istrazivaciPage.istrazivaciTable.isDisplayed()).toBe(true);
    expect(istrazivaciPage.dodajIstrazivacaBtn.isDisplayed()).toBe(true);

    istrazivaciPage.dodajIstrazivacaBtn.click();

//  da ;i su oni gornji dugmici tu
    expect(createIstrazivaci.licniPodaci.isPresent());
    expect(createIstrazivaci.licniPodaci.isDisplayed());
    expect(createIstrazivaci.licniPodaci.isEnabled());
    expect(createIstrazivaci.podaciRegistar.isPresent());
    expect(createIstrazivaci.podaciRegistar.isDisplayed());
    expect(createIstrazivaci.podaciRegistar.isEnabled());
    expect(createIstrazivaci.podaciZaProjekte.isPresent());
    expect(createIstrazivaci.podaciZaProjekte.isDisplayed());
    expect(createIstrazivaci.podaciZaProjekte.isEnabled());

  

    //provjera gresaka
    createIstrazivaci.sacuvajBtn.click();
    expect(createIstrazivaci.imeError.isPresent()).toBe(true);
    expect(createIstrazivaci.imeError.isDisplayed()).toBe(true);
    expect(createIstrazivaci.prezimeError.isPresent()).toBe(true);
    expect(createIstrazivaci.prezimeError.isDisplayed()).toBe(false);
    expect(createIstrazivaci.jmbgError.isPresent()).toBe(true);
    expect(createIstrazivaci.jmbgError.isDisplayed()).toBe(false);


  
  createIstrazivaci.popuniCijeluFormu("Andrej", "Milic", "JOvan", "Mr", "05.10.2015", "Srbija", "NoviSad", "NoviSad", "Srbija", "Beograd", "Beograd", "adresa", "Muški", "955555689229091", "ana@gmail.com", "345", "www", "Saradnik");
  
  // provjera koji pol je izabran
  var pol =createIstrazivaci.izabraniPol.getText();
  expect(pol).toEqual("Muški");

  createIstrazivaci.sacuvajBtn.click();


  browser.wait(function() {
        return succesModalPage.succesModalAppears.isDisplayed();
    }, 3000);
    succesModalPage.succesModalAppears.click();


    //Potvrda da se nakon klika na "Sačuvaj" otvara zeleni Modal "success"
    expect(succesModalPage.succesModalAppears.isPresent()).toBe(true);
    expect(succesModalPage.succesModalAppears.isDisplayed()).toBe(true);

    succesModalPage.pauseBtn.click();
    succesModalPage.xBtn.click();


    // //pa se na "x" zatvori
    // succesModalPage.exitModal();


//Cekam da modal nestane

// Sacekati da se modal skloni sa stranice
    browser.wait(function() {
      return succesModalPage.succesModalAppears.isPresent().then(function(isPresent) {
        return !isPresent;
      });
    }, 5000, 'Modal dialog never disappeared');

expect(succesModalPage.succesModalAppears.isPresent()).toBe(false);

//provera na modalu angazuj
  //expect(angazujPage.naslov.isPresent()).toBe(true);
  //expect(angazujPage.naslov.isDisplayed()).toBe(true);
  expect(angazujPage.zvanje.isDisplayed()).toBe(true);
  expect(angazujPage.funkcija.isDisplayed()).toBe(true);
  expect(angazujPage.datumOd.isDisplayed()).toBe(true);
  expect(angazujPage.datumDo.isDisplayed()).toBe(true);
 

  expect(angazujPage.angazujBtn.isDisplayed()).toBe(true);
  expect(angazujPage.angazujBtn.isEnabled()).toBe(true);

  //provera errora
  angazujPage.angazujBtn.click();
  expect(angazujPage.datumOdError.isDisplayed()).toBe(true);

  //setovanje
  angazujPage.zvanje = "Asistent";
  angazujPage.datumOd = "14.07.2015";

  angazujPage.angazujBtn.click();



  //ceka se SUCCESS modal

  browser.wait(function() {
        return succesModalPage.succesModalAppears.isDisplayed();
    }, 3000);
    succesModalPage.succesModalAppears.click();


  //provera modal
  expect(succesModalPage.succesModalAppears.isPresent()).toBe(true);
  expect(succesModalPage.succesModalAppears.isDisplayed()).toBe(true);


  succesModalPage.pauseBtn.click();
    succesModalPage.xBtn.click();
    // //pa se na "x" zatvori
    // succesModalPage.exitModal();



// Sacekati da se modal skloni sa stranice
    browser.wait(function() {
      return succesModalPage.succesModalAppears.isPresent().then(function(isPresent) {
        return !isPresent;
      });
    }, 5000, 'Modal dialog never disappeared');

expect(succesModalPage.succesModalAppears.isPresent()).toBe(false);

//vraca se na istrazivace
expect(minisPage.istrazivaciBtn.isDisplayed());
expect(minisPage.istrazivaciBtn.isEnabled());
minisPage.istrazivaciBtn.click();

expect(istrazivaciPage.istrazivaciTable.isPresent()).toBe(true);
expect(istrazivaciPage.istrazivaciTable.isDisplayed()).toBe(true);



expect(istrazivaciPage.istrazivacRowByPrezime("Milic").isPresent()).toBe(true);


// //edit
// istrazivaciPage.editIstrazivacByPrezime("bobicevic");


    

    // IstrazivaciCreation.podaciRegistar.click();





  });

});