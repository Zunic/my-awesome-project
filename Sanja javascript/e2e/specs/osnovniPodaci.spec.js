var LoginPage = require('../page_objects/logIn.page.js');
var OsnovniPodaciPage = require('../page_objects/osnovniPodaci.page.js');
var SuccesModalPage = require('../page_objects/succesModal.page.js');

describe('Login page:', function() {
  var loginPage;
  var osnovniPodaciPage;
  var succesModalPage;

  beforeAll(function() {
    browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
    loginPage = new LoginPage();
    osnovniPodaciPage = new OsnovniPodaciPage();
    succesModalPage = new SuccesModalPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://park.ftn.uns.ac.rs:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {

// cekanje da se pojavi login stranica
    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "11223344";
    loginPage.prijaviSeBtn.click();

     browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/admin-institution/';
      });
    }, 5000, 'Could not navigate to admin');


  });

  it('Unos osnovnih podataka."', function() {

    expect(osnovniPodaciPage.nazivInstitucije.isPresent()).toBe(true);
    expect(osnovniPodaciPage.nazivInstitucije.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.nazivInstitucijeEng.isPresent()).toBe(true);
    expect(osnovniPodaciPage.nazivInstitucijeEng.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.drzava.isPresent()).toBe(true);
    expect(osnovniPodaciPage.drzava.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.mesto.isPresent()).toBe(true);
    expect(osnovniPodaciPage.mesto.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.opstina.isPresent()).toBe(true);
    expect(osnovniPodaciPage.opstina.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.ulicaIBroj.isPresent()).toBe(true);
    expect(osnovniPodaciPage.ulicaIBroj.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.webAdresa.isPresent()).toBe(true);
    expect(osnovniPodaciPage.webAdresa.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.elektronskaPosta.isPresent()).toBe(true);
    expect(osnovniPodaciPage.elektronskaPosta.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.telefon.isPresent()).toBe(true);
    expect(osnovniPodaciPage.telefon.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.skraceniNaziv.isPresent()).toBe(true);
    expect(osnovniPodaciPage.skraceniNaziv.isDisplayed()).toBe(true);
    // expect(osnovniPodaciPage.institucijaDDklik.isPresent()).toBe(true);
    // expect(osnovniPodaciPage.institucijaDDklik.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.odustaniBtn.isPresent()).toBe(true);
    expect(osnovniPodaciPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.sacuvajBtn.isPresent()).toBe(true);
    expect(osnovniPodaciPage.sacuvajBtn.isDisplayed()).toBe(true);

    osnovniPodaciPage.nazivInstitucije.clear();
    // osnovniPodaciPage.nazivInstitucije = "";
    osnovniPodaciPage.mesto = "";
    osnovniPodaciPage.ulicaIBroj = "";
    osnovniPodaciPage.webAdresa = "";
    osnovniPodaciPage.elektronskaPosta = "";
    osnovniPodaciPage.telefon = "";

    expect(osnovniPodaciPage.nazivInstitucijeError.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.mestoError.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.ulicaIBrojError.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.webAdresaError.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.elektronskaPostaError.isDisplayed()).toBe(true);
    expect(osnovniPodaciPage.telefonError.isDisplayed()).toBe(true);
   
    osnovniPodaciPage.drzava = "Dodaj novu...";
    osnovniPodaciPage.novaDrzava = "Mauricijus";
    osnovniPodaciPage.opisNoveDrzave = "";
    osnovniPodaciPage.tacnoBtn.click();


   osnovniPodaciPage.nazivInstitucije = "Sanja Karan Institucija";
   osnovniPodaciPage.ulicaIBroj = "Temerinska 23";
   osnovniPodaciPage.webAdresa ="www.google.com";
   osnovniPodaciPage.elektronskaPosta = "sanja@yahoo.com"
    osnovniPodaciPage.drzava = "Mauricijus";
    osnovniPodaciPage.telefon = "123455667";
    osnovniPodaciPage.mesto = "Beograd";
    // osnovniPodaciPage.institucijaDDklik.click()
    // osnovniPodaciPage.sadKlikNaInstituciju("Neda Raspopovic");
    
    // expect(osnovniPodaciPage.provjeraSelekcije.isSelected()).toBe(true);

    osnovniPodaciPage.sacuvajBtn.click();


// cekanje da se pojavi dijalog SAcuvani podaci
      browser.wait(function() {
        return succesModalPage.succesModalAppears.isDisplayed();
    }, 3000);
    succesModalPage.succesModalAppears.click();

    //Potvrda da se nakon klika na "Sačuvaj" otvara zeleni Modal "success"
    expect(succesModalPage.succesModalAppears.isDisplayed()).toBe(true);
    expect(succesModalPage.succesModalAppears.isPresent()).toBe(true);

    //Na modalu se klikne na pauzu
    succesModalPage.pauseBtn.click();

    browser.sleep(5000);

    //pa se na modalu klikne na "play"
    succesModalPage.playBtn.click();

    //pa se na "x" zatvori
    succesModalPage.xBtn.click();


  });

});