var LoginPage = require('../page_objects/logIn.page.js');
var MinisPage = require('../page_objects/minis.page.js');
var PodaciZaProjektePage = require('../page_objects/podaciZaProjekte.page.js');
var SuccesModalPage = require('../page_objects/succesModal.page.js');

describe('Login page:', function() {
  var loginPage;
  var minisPage;
  var podaciZaProjektePage;
  var succesModalPage;

  beforeAll(function() {
    browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
    loginPage = new LoginPage();
    minisPage = new MinisPage();
    podaciZaProjektePage = new PodaciZaProjektePage();
    succesModalPage = new SuccesModalPage();

   expect(browser.getCurrentUrl()).toEqual('http://park.ftn.uns.ac.rs:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "11223344";
    loginPage.prijaviSeBtn.click();

     browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/admin-institution/';
      });
    }, 5000, 'Could not navigate to login');

  });

   it('projekti.', function() {
       minisPage.podaciZaProjekte.click();

// provjera da li su sva polja prikazana
        expect(podaciZaProjektePage.brojRacuna.isPresent()).toBe(true);
        expect(podaciZaProjektePage.brojRacuna.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.idBrojUInostranstvu.isPresent()).toBe(true);
        expect(podaciZaProjektePage.idBrojUInostranstvu.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.idMedjunarodniNivo.isPresent()).toBe(true);
        expect(podaciZaProjektePage.idMedjunarodniNivo.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.statusInstitucije.isPresent()).toBe(true);
        expect(podaciZaProjektePage.statusInstitucije.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.oblastIstrazivanja.isPresent()).toBe(true);
        expect(podaciZaProjektePage.oblastIstrazivanja.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.odustaniBtn.isPresent()).toBe(true);
        expect(podaciZaProjektePage.odustaniBtn.isDisplayed()).toBe(true);
        expect(podaciZaProjektePage.sacuvajBtn.isPresent()).toBe(true);
        expect(podaciZaProjektePage.sacuvajBtn.isDisplayed()).toBe(true);
        
// provjera da li javlja greske za prazna polja
        podaciZaProjektePage.brojRacuna.clear();
        podaciZaProjektePage.idMedjunarodniNivo.clear();
        podaciZaProjektePage.idMedjunarodniNivo.clear();
        podaciZaProjektePage.sacuvajBtn.click();
        expect(podaciZaProjektePage.brojRacunaError.isDisplayed()).toBe(true);
        // expect(podaciZaProjektePage.statusInstitucijeError.isDisplayed()).toBe(true);

// provjera da li javlja gresku o formatu broja racuna
        podaciZaProjektePage.brojRacuna = "dsds";
        expect(podaciZaProjektePage.brojRacunaFormatError.isDisplayed()).toBe(true);

// provjera kada je format broja racuna ispravana
        podaciZaProjektePage.brojRacuna.clear();
        podaciZaProjektePage.brojRacuna = "123-1234567890123-12";
        expect(podaciZaProjektePage.brojRacunaError.isDisplayed()).toBe(false);
        expect(podaciZaProjektePage.brojRacunaFormatError.isDisplayed()).toBe(false);
        
         podaciZaProjektePage.idMedjunarodniNivo = "123";
         podaciZaProjektePage.statusInstitucije = "Status";
         podaciZaProjektePage.sacuvajBtn.click();


         // cekanje da se pojavi dijalog SAcuvani podaci
      browser.wait(function() {
        return succesModalPage.succesModalAppears.isDisplayed();
    }, 3000);
    succesModalPage.succesModalAppears.click();

    //Potvrda da se nakon klika na "Sačuvaj" otvara zeleni Modal "success"
    expect(succesModalPage.succesModalAppears.isDisplayed()).toBe(true);
    expect(succesModalPage.succesModalAppears.isPresent()).toBe(true);

    //Na modalu se klikne na pauzu
    succesModalPage.pauseBtn.click();

    browser.sleep(5000);

    // //pa se na modalu klikne na "play"
    // succesModalPage.playBtn.click();

    //pa se na "x" zatvori
    succesModalPage.xBtn.click();


   });

});