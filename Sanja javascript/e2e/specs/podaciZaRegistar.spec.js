var LoginPage = require('../page_objects/logIn.page.js');
var OsnovniPodaciPage = require('../page_objects/osnovniPodaci.page.js');
var MinisPage = require('../page_objects/minis.page.js');
var PodaciZaRegistarPage = require('../page_objects/podaciZaRegistar.page.js');


describe('Login page:', function() {
  var loginPage;
  var osnovniPodaciPage;
  var minisPage;
  var podaciZaRegistarPage;


  beforeAll(function() {
    browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
    loginPage = new LoginPage();
    osnovniPodaciPage = new OsnovniPodaciPage();
    minisPage = new MinisPage();
    podaciZaRegistarPage = new PodaciZaRegistarPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://park.ftn.uns.ac.rs:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "11223344";
    loginPage.prijaviSeBtn.click();

     browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://park.ftn.uns.ac.rs:8080/#/admin-institution/';
      });
    }, 5000, 'Could not navigate to login');

  });

   it('Registar.', function() {
       minisPage.podaciZaRegistar.click();

        expect(podaciZaRegistarPage.poreskiID.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.poreskiID.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.maticniBroj.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.maticniBroj.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.brPoslednjeNaucneAkred.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.brPoslednjeNaucneAkred.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.datumPoslednjeNaucneAkred.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.datumPoslednjeNaucneAkred.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.nazivInstitucijeIzAkred.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.nazivInstitucijeIzAkred.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.napomenaORegistru.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.napomenaORegistru.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.vrstaInstitucije.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.vrstaInstitucije.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.osnovnaDelatnostInstitucije.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.osnovnaDelatnostInstitucije.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.osnivac.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.osnivac.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.datumOsnivanja.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.datumOsnivanja.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.brojResenjaOOsnvanju.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.brojResenjaOOsnvanju.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.vlasnickaStruktura.isPresent()).toBe(true);
        expect(podaciZaRegistarPage.vlasnickaStruktura.isDisplayed()).toBe(true);
      

//  provjera kada su sva polja prazna (da li prikazuje greske)
        podaciZaRegistarPage.sacuvajBtn.click();
        expect(podaciZaRegistarPage.poreskiIDError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.maticniBrojError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.brPoslednjeNaucneAkredError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.datumPoslednjeNaucneAkredError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.nazivInstitucijeIzAkredError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.vrstaInstitucijeError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.vlasnickaStrukturaError.isDisplayed()).toBe(true);

// provjera da li prikazuje greske kada su pogresni formati
        podaciZaRegistarPage.poreskiID = "aaa";
        podaciZaRegistarPage.maticniBroj = "asdf";

        expect(podaciZaRegistarPage.maticniBrojFormatError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.poreskiIDFormatError.isDisplayed()).toBe(true);

//  ispravan unos podataka
       
        podaciZaRegistarPage.poreskiID = "12345";
        podaciZaRegistarPage.maticniBroj = "1234567890123";
        podaciZaRegistarPage.brPoslednjeNaucneAkred = "23";
        podaciZaRegistarPage.datumPoslednjeNaucneAkred = "1.6.2016";
        podaciZaRegistarPage.nazivInstitucijeIzAkred = "Instutucija Gala";
        podaciZaRegistarPage.napomenaORegistru = "Napomene su super";
        podaciZaRegistarPage.osnivac = "Sanja Karan";
        podaciZaRegistarPage.brojResenjaOOsnvanju = "456783";
        podaciZaRegistarPage.vlasnickaStruktura = "Privatna";

        podaciZaRegistarPage.sacuvajBtn.click();

        // ove greske se prikazuju jer ne mogu da selektujem nista
        expect(podaciZaRegistarPage.vrstaInstitucijeError.isDisplayed()).toBe(true);
        
        // greske se ne prikazuju
        expect(podaciZaRegistarPage.poreskiIDError.isDisplayed()).toBe(false);
        expect(podaciZaRegistarPage.maticniBrojError.isDisplayed()).toBe(false);
        expect(podaciZaRegistarPage.brPoslednjeNaucneAkredError.isDisplayed()).toBe(false);
        expect(podaciZaRegistarPage.datumPoslednjeNaucneAkredError.isDisplayed()).toBe(true);
        expect(podaciZaRegistarPage.nazivInstitucijeIzAkredError.isDisplayed()).toBe(false);
        expect(podaciZaRegistarPage.vlasnickaStrukturaError.isDisplayed()).toBe(false);
        expect(podaciZaRegistarPage.vlasnickaStrukturaError.isDisplayed()).toBe(false);


      
  });

});