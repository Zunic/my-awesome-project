var LoginPage = require('../page_objects/logIn.page.js');
var MinisPage = require('../page_objects/minis.page.js');
var IstrazivaciPage = require('../page_objects/istrazivaci.page.js');
var CreateIstrazivaciPage = require('../page_objects/createIstrazivaci.page.js');
var AngazujPage = require('../page_objects/angazuj.page.js');

describe('Login page:', function() {
  var loginPage;
  var minisPage;
  var istrazivaciPage;
  var createIstrazivaciPage;
  var angazujPage;

  beforeAll(function() {
    browser.navigate().to("http://192.168.58.25:8080/#/login");
    loginPage = new LoginPage();
    minisPage = new MinisPage();
    istrazivaciPage = new IstrazivaciPage();
    createIstrazivaciPage = new CreateIstrazivaciPage();
    angazujPage = new AngazujPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://192.168.58.25:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://192.168.58.25:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

// logovanje sa ispravnim username-om i password-om
    loginPage.username.clear();
    loginPage.password.clear();

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "dpWPBbYGGWgxGuOaGlrA";
    loginPage.prijaviSeBtn.click();

// provjera a li je dugme tu
        expect(minisPage.istrazivaciBtn.isPresent()).toBe(true);
        expect(minisPage.istrazivaciBtn.isDisplayed()).toBe(true);
        expect(minisPage.istrazivaciBtn.isEnabled()).toBe(true);

     minisPage.istrazivaciBtn.click();

     istrazivaciPage.istrazivacRowByIme("Novi1222_protractor").click();

      });
    });