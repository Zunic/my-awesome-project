var LoginPage = require('../page_objects/logIn.page.js');
var MinisPage = require('../page_objects/minis.page.js');

describe('Login page:', function() {
  var loginPage;
  var minisPage;

  beforeAll(function() {
    browser.navigate().to("http://192.168.58.25:8080/#/login");
    loginPage = new LoginPage();
    minisPage = new MinisPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://192.168.58.25:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://192.168.58.25:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

// provjera da li javlja gresku kad se unesu pogresan username ili password
    loginPage.username = "a";
    loginPage.password = "a";
    loginPage.prijaviSeBtn.click();

    var expectedMessage = "Pogrešno korisničko ime ili lozinka!";
    
    expect(loginPage.loginProblem).toEqual(expectedMessage);

// logovanje sa ispravnim username-om i password-om
    loginPage.username.clear();
    loginPage.password.clear();

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "11223344";
    loginPage.prijaviSeBtn.click();

     
  });