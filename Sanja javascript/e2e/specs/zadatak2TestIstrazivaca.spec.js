var LoginPage = require('../page_objects/logIn.page.js');
var MinisPage = require('../page_objects/minis.page.js');
var IstrazivaciPage = require('../page_objects/istrazivaci.page.js');
var CreateIstrazivaciPage = require('../page_objects/createIstrazivaci.page.js');
var AngazujPage = require('../page_objects/angazuj.page.js');

describe('Login page:', function() {
  var loginPage;
  var minisPage;
  var istrazivaciPage;
  var createIstrazivaciPage;
  var angazujPage;

  beforeAll(function() {
    browser.navigate().to("http://192.168.58.25:8080/#/login");
    loginPage = new LoginPage();
    minisPage = new MinisPage();
    istrazivaciPage = new IstrazivaciPage();
    createIstrazivaciPage = new CreateIstrazivaciPage();
    angazujPage = new AngazujPage();
   
   expect(browser.getCurrentUrl()).toEqual('http://192.168.58.25:8080/#/login');
  });

  it('should successfully log in as "sanja1113"', function() {


    browser.wait(function() {
      return browser.getCurrentUrl().then(function(url) {
        return url === 'http://192.168.58.25:8080/#/login';
      });
    }, 5000, 'Could not navigate to login');
    
    // provjera da se "prijaviSeBtn" ne moze pritisnuti prije nekog unosa
    expect(loginPage.prijaviSeBtn.isEnabled()).toBe(false);


    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.odustaniBtn.isPresent()).toBe(true);
    expect(loginPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSeBtn.isPresent()).toBe(true);
    expect(loginPage.prijaviSeBtn.isDisplayed()).toBe(true);

// logovanje sa ispravnim username-om i password-om
    loginPage.username.clear();
    loginPage.password.clear();

    loginPage.username = "sanja1113@yahoo.com";
    loginPage.password = "dpWPBbYGGWgxGuOaGlrA";
    loginPage.prijaviSeBtn.click();

// provjera a li je dugme tu
        expect(minisPage.istrazivaciBtn.isPresent()).toBe(true);
        expect(minisPage.istrazivaciBtn.isDisplayed()).toBe(true);
        expect(minisPage.istrazivaciBtn.isEnabled()).toBe(true);

     minisPage.istrazivaciBtn.click();

    //  cekanje da se ucitaju istrazivaci
      browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                 return url === 'http://192.168.58.25:8080/#/persons';
      });
    }, 5000, 'Istrazivaci stranica ne može da se učita');

// da li su se ucitali
    expect(istrazivaciPage.istrazivaciTable.isDisplayed()).toBe(true);
    expect(istrazivaciPage.dodajIstrazivacaBtn.isDisplayed()).toBe(true);

//  prelazak na dodaj istrazivaca
    istrazivaciPage.dodajIstrazivacaBtn.click();

    expect(createIstrazivaciPage.ime.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.prezime.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.imeRoditelja.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.titulaIstrazivaca.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.datumRodjenja.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.drzavaRodjenja.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.mestoRodjenja.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.opstinaRodjenja.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.drzavaBoravista.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.mestoBoravista.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.opstinaBoravista.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.ulicaIBrojBoravista.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.pol.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.jmbg.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.elektronskaPosta.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.telefon.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.licnaVebAdresa.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.statusIstrazivaca.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.odustaniBtn.isDisplayed()).toBe(true);
    expect(createIstrazivaciPage.sacuvajBtn.isDisplayed()).toBe(true);

    createIstrazivaciPage.ime = "Novi1222_protractor";
    createIstrazivaciPage.prezime = "Istrazivac";
    createIstrazivaciPage.jmbg = "123555559";
    createIstrazivaciPage.sacuvajBtn.click();

    expect(angazujPage.naslov.isDisplayed()).toBe(true);
    angazujPage.odustaniBtn.click();
    
    browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                 return url === 'http://192.168.58.25:8080/#/persons/';
      });
    }, 5000, 'Istrazivaci stranica ne može da se učita');


//  STRANICA SA istrazivacima
    minisPage.istrazivaciBtn.click();

      browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                 return url === 'http://192.168.58.25:8080/#/persons';
      });
    }, 5000, 'Istrazivaci stranica ne može da se učita');

// da li su se ucitali
    expect(istrazivaciPage.istrazivaciTable.isDisplayed()).toBe(true);
    expect(istrazivaciPage.dodajIstrazivacaBtn.isDisplayed()).toBe(true);

    // expect(istrazivaciPage.istrazivacRowByIme("Novi1222_protractor").isPresent()).toBe(true);

    // odjava
     minisPage.zastavica.click();
     minisPage.odjava.click();

  });
    });