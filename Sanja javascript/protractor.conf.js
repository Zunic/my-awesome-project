var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var SpecReporter = require('jasmine-spec-reporter');

exports.config = {
    seleniumServerJar: '../../../node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    chromeDriver: '../../../node_modules/protractor/selenium/chromedriver',
    allScriptsTimeout: 20000,

    specs: [
                // 'e2e/specs/logIn.spec.js'
                //    'e2e/specs/osnovniPodaci.spec.js'
                //  'e2e/specs/podaciZaRegistar.spec.js'
                //   'e2e/specs/podaciZaProjekte.spec.js'
                //  'e2e/specs/istrazivaciTest.spec.js'
                // 'e2e/specs/zadatak2TestIstrazivaca.spec.js'
                'e2e/specs/zadatak2AngazovanjaTest.spec.js'
    ],

    capabilities: {
        'browserName': 'chrome',
    },

    directConnect: true,

    baseUrl: 'http://localhost:8080/#/',

    framework: 'jasmine2',

    jasmineNodeOpts: {
        showColors: true,
        isVerbose: true,
        defaultTimeoutInterval: 50000,
        print: function() {}
    },

    onPrepare: function() {
        // Postavljamo prozor na fullscreen
        browser.driver.manage().window().maximize();

        // Registrujemo reportere
        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: "./target/reports/e2e/",
            takeScreenshots: true,
            takeScreenshotsOnlyOnFailures: true,
            fixedScreenshotName: true,
        }));
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'all',
            displaySpecDuration: true,
            displayFailuresSummary: false,
            displayPendingSummary: false,
        }));
    }
};
