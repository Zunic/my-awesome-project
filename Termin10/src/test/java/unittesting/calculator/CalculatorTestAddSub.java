package unittesting.calculator;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTestAddSub {

	Calculator calc = new Calculator();
	
	@Test(groups = "Odabrana metoda")
	public void TestAdd() {
		
		calc.add(5);
		assertEquals(5, calc.getResult());
	}
	
	@Test
	public void TestSub() {
		
		calc.add(10);
		calc.substract(5);		
		assertEquals(5, calc.getResult());		
	}
	
	@BeforeMethod
	public void Clear() {
		calc.clear();
	}
	
	@BeforeGroups(groups = {"Odabrana metoda"})
	public void Before() {
		System.out.println("Sada će se pokrenuti svi testovi iz grupe ODABRANIH METODA.");
	}
	
	@AfterGroups(groups = {"Odabrana metoda"})
	public void After() {
		System.out.println("Svi testovi iz grupe ODABRANIH METODA su izvršeni.");
	}

}
