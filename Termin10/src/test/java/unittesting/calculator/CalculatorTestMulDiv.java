package unittesting.calculator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTestMulDiv {

	Calculator calc = new Calculator();
	
	@Test(enabled = false)
	public void TestMult0ByNot0() {
		
		calc.multiply(5);
		assertEquals(0, calc.getResult());		
	}
	
	@Test(enabled = false)
	public void TestMultNot0ByNot0() {
		
		calc.add(5);
		calc.multiply(2);
		assertEquals(10, calc.getResult());
	}
	
	@Test(enabled = false)
	public void TestMultNot0By0() {
		
		calc.add(5);
		calc.multiply(0);
		assertEquals(0, calc.getResult());
	}
	
	@Test(enabled = false)
	public void TestMult0By0() {
		
		calc.multiply(0);
		assertEquals(0, calc.getResult());
	}
	
	@Test
	public void TestDiv0ByNot0() {
		
		calc.divide(2);
		assertEquals(0, calc.getResult());
	}
	
	@Test
	public void TestDivNot0ByNot0() {
		
		calc.add(10);
		calc.divide(2);
		assertEquals(5, calc.getResult());
	}
	
	@Test(expectedExceptions = java.lang.ArithmeticException.class)
	public void TestDiv0By0() {
		
		calc.divide(0);
	}
	
	@Test(expectedExceptions = java.lang.ArithmeticException.class)
	public void TestDivNot0By0() {
		
		calc.add(10);
		calc.divide(0);
	}
		
	@BeforeMethod
	public void Clear() {
		calc.clear();
	}
}
