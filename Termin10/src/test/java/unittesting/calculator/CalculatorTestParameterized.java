package unittesting.calculator;

import static org.junit.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class CalculatorTestParameterized {

	Calculator calc = new Calculator();
	
	@Test(groups = "Odabrana metoda")
	@Parameters({ "parametarZaSabiranje" })
	public void TestAdd(int parametarZaSabiranje) {
		
		calc.add(parametarZaSabiranje);
		assertEquals(5, calc.getResult());
	}
	
	@Test
	@Parameters({ "parametarZaOduzimanje" })
	public void TestSub(int parametarZaOduzimanje) {
		
		calc.add(10);
		calc.substract(parametarZaOduzimanje);		
		assertEquals(1, calc.getResult());		
	}
	
	@Test(enabled = false)
	@Parameters({ "parametarZaMnozenje" })
	public void TestMult0ByNot0(int parametarZaMnozenje) {
		
		calc.multiply(parametarZaMnozenje);
		assertEquals(0, calc.getResult());		
	}
	
	@Test(enabled = false)
	@Parameters({ "parametarZaMnozenje" })
	public void TestMultNot0ByNot0(int parametarZaMnozenje) {
		
		calc.add(5);
		calc.multiply(parametarZaMnozenje);
		assertEquals(10, calc.getResult());
	}
	
	@Test(enabled = false)
	public void TestMultNot0By0() {
		
		calc.add(5);
		calc.multiply(0);
		assertEquals(0, calc.getResult());
	}
	
	@Test(enabled = false)
	public void TestMult0By0() {
		
		calc.multiply(0);
		assertEquals(0, calc.getResult());
	}
	
	@Test
	@Parameters({ "parametarZaDeljenje" })
	public void TestDiv0ByNot0(int parametarZaDeljenje) {
		
		calc.divide(parametarZaDeljenje);
		assertEquals(0, calc.getResult());
	}
	
	@Test
	@Parameters({ "parametarZaDeljenje" })
	public void TestDivNot0ByNot0(int parametarZaDeljenje) {
		
		calc.add(10);
		calc.divide(parametarZaDeljenje);
		assertEquals(5, calc.getResult());
	}
	
	@Test(expectedExceptions = java.lang.ArithmeticException.class)
	public void TestDiv0By0() {
		
		calc.divide(0);
	}
	
	@Test(expectedExceptions = java.lang.ArithmeticException.class)
	public void TestDivNot0By0() {
		
		calc.add(10);
		calc.divide(0);
	}

	@Test(groups = "Odabrana metoda")
	@Parameters({ "parametarZaKvadriranje" })
	public void TestSqNot0(int parametarZaKvadriranje) {
		
		calc.square(parametarZaKvadriranje);
		assertEquals(64, calc.getResult());
	}
	
	@Test(groups = "Odabrana metoda")
	public void TestSq0() {
		
		calc.square(0);
		assertEquals(0, calc.getResult());
	}
	
	
	@BeforeMethod
	public void Clear() {
		calc.clear();
	}
}
