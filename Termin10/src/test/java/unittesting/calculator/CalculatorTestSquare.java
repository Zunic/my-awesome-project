package unittesting.calculator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import static org.junit.Assert.assertEquals;

public class CalculatorTestSquare {

	Calculator calc = new Calculator();
	
	@Test(groups = "Odabrana metoda")
	public void TestSqNot0() {
		
		calc.square(2);
		assertEquals(4, calc.getResult());
	}
	
	@Test(groups = "Odabrana metoda")
	public void TestSq0() {
		
		calc.square(0);
		assertEquals(0, calc.getResult());
	}
			
	@BeforeMethod
	public void Clear() {
		calc.clear();
	}
}
