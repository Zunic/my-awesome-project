var CreateOrEditStudentPage = function() {};

CreateOrEditStudentPage.prototype = Object.create({}, {

    indeks: {
        get: function() {
            return element(by.id("field_indeks"));
        },
        set: function(value) {
            this.indeks.clear();
            this.indeks.sendKeys(value);
        }
    },

    ime: {
        get: function() {
            return element(by.id("field_ime"));
        },
        set: function(value) {
            this.ime.clear();
            this.ime.sendKeys(value);
        }
    },

    prezime: {
        get: function() {
            return element(by.id("field_prezime"));
        },
        set: function(value) {
            this.prezime.clear();
            this.prezime.sendKeys(value);
        }
    },
    
    grad: {
        get: function() {
            return element(by.id("field_grad"));
        },
        set: function(value) {
            this.grad.clear();
            this.grad.sendKeys(value);
        }
    },

    createOrEditStudent: {
        value: function(indeksString, imeString, prezimeString, gradString) {
            this.indeks = indeksString;
            this.ime = imeString;
            this.prezime = prezimeString;
            this.grad = gradString;
        }
    }
 
});

module.exports = CreateOrEditStudentPage;