var EntitiesPage = function() {};

EntitiesPage.prototype = Object.create({}, {

entitiesMenu: {
    get: function() {
        return element(by.xpath("//span [@translate='global.menu.entities.main']"))
    }
},

ispitniRokovi: {
    get: function() {
        return element(by.xpath("//a [@ui-sref='ispitniRokovi']"))
    }
},

nastavnici: {
    get: function() {
        return element(by.xpath("//a [@ui-sref='nastavnici']"))
    }
},

studenti: {
    get: function() {
        return element(by.xpath("//a [@ui-sref='studenti']"))
    }
},

predmeti: {
    get: function() {
        return element(by.xpath("//a [@ui-sref='predmeti']"))
    }
},

ispitnePrijeve: {
    get: function() {
        return element(by.xpath("//a [@ui-sref='ispitnePrijeve']"))
    }
},

});

module.exports = EntitiesPage;