var ModalWindowPage = function() {};

ModalWindowPage.prototype = Object.create({}, {

    getCancelButton: {
        get: function() {
            var modalFooter = element(by.xpath("//div [@class='modal-footer']"));
            var cancelButton = modalFooter.element(by.xpath("//button [@class='btn-default']"))
            return cancelButton;
        }
    },

    getSaveButton: {
        get: function() {
            var modalFooter = element(by.xpath("//div [@class='modal-footer']"));
            var saveButton = modalFooter.element(by.xpath("/html/body/div[5]/div/div/form/div[3]/button[2]"))
            return saveButton;
        }
    },

    getDeleteButton: {
        get: function() {
            var modalFooter = element(by.xpath("//div [@class='modal-footer']"));
            var deleteButton = modalFooter.element(by.xpath("/html/body/div[5]/div/div/form/div[3]/button[2]"))
            return deleteButton;
        }
    },

});

module.exports = ModalWindowPage;