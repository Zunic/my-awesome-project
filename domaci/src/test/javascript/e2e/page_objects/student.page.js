var StudentPage = function() {};

StudentPage.prototype = Object.create({}, {

    getCreateStudent: {
        get: function() {
            return element(by.xpath("//button [@ui-sref='studenti.new']"))
        }
    },

    getStudentRow: {
        value: function(indeks) {
            var studentRow = element(by.xpath("//a [text()='" + indeks + "']/../.."));
            return studentRow;
        }
    },

    getDeleteButton: {
        value: function(indeks) {
            var studentRow = element(by.xpath("//a [text()='" + indeks + "']/../.."));
            return studentRow.element(by.className("btn-danger"))
        }
    },

    getViewButton: {
        value: function(indeks) {
            var studentRow = element(by.xpath("//a [text()='" + indeks + "']/../.."));
            return studentRow.element(by.className("btn-info"))
        }
    },

    getEditButton: {
        value: function(indeks) {
            var studentRow = element(by.xpath("//a [text()='" + indeks + "']/../.."));
            return studentRow.element(by.className("btn-primary"))
        }
    },

    navigateToPage: { 
        value: function() {
            browser.get('https://localhost:8080/#/studentis')
        }
    }
});

module.exports = StudentPage;
