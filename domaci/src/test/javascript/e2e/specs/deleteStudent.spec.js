var LoginPage = require('../page_objects/login.page.js');
var MenuPage = require('../page_objects/menu.page.js');
var StudentPage = require('../page_objects/student.page.js');
var CreateOrEditStudentPage = require('../page_objects/createOrEditStudent.page.js');
var ModalWindowPage = require('../page_objects/modalWindow.page.js');
var EntitiesPage = require('../page_objects/entities.page.js');

describe('Delete student:', function() {
    var loginPage;
    var menuPage;
    var studentPage;
    var createOrEditStudentPage;
    var modalWindowPage;
    var entitiesPage;

    beforeAll(function() {
        browser.navigate().to("localhost:8080/#/");
        loginPage = new LoginPage();
        menuPage = new MenuPage();
        studentPage = new StudentPage();
        createOrEditStudentPage = new CreateOrEditStudentPage();
        modalWindowPage = new ModalWindowPage();
        entitiesPage = new EntitiesPage();
    });

    it('should successfully delete a student', function() {

        menuPage.accountMenu.click();

        expect(menuPage.signIn.isPresent()).toBe(true);
        expect(menuPage.signIn.isDisplayed()).toBe(true);

        menuPage.signIn.click();

        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

        loginPage.login('admin', 'admin');

        entitiesPage.entitiesMenu.click();

        expect(entitiesPage.studenti.isPresent()).toBe(true);
        expect(entitiesPage.studenti.isDisplayed()).toBe(true);

        entitiesPage.studenti.click();

        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/studentis');

        studentPage.getDeleteButton('E1234').click();
        modalWindowPage.getDeleteButton.click();

        var EC = protractor.ExpectedConditions;
        var stRow = studentPage.getStudentRow('E1234');

        expect(browser.wait(EC.not(EC.visibilityOf(stRow)), 5000)).toBe(true);

        menuPage.accountMenu.click();
        menuPage.logOut.click();
    });
});