var LoginPage = require('../page_objects/login.page.js');
var MenuPage = require('../page_objects/menu.page.js');
var StudentPage = require('../page_objects/student.page.js');
var CreateOrEditStudentPage = require('../page_objects/createOrEditStudent.page.js');
var ModalWindowPage = require('../page_objects/modalWindow.page.js');
var EntitiesPage = require('../page_objects/entities.page.js');

describe('Edit student:', function() {
    var loginPage;
    var menuPage;
    var studentPage;
    var createOrEditStudentPage;
    var modalWindowPage;
    var entitiesPage;

    beforeAll(function() {
        browser.navigate().to("localhost:8080/#/");
        loginPage = new LoginPage();
        menuPage = new MenuPage();
        studentPage = new StudentPage();
        createOrEditStudentPage = new CreateOrEditStudentPage();
        modalWindowPage = new ModalWindowPage();
        entitiesPage = new EntitiesPage();
    });

    it('should successfully edit a student', function() {

        menuPage.accountMenu.click();

        expect(menuPage.signIn.isPresent()).toBe(true);
        expect(menuPage.signIn.isDisplayed()).toBe(true);

        menuPage.signIn.click();

        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

        loginPage.login('admin', 'admin');

        entitiesPage.entitiesMenu.click();

        expect(entitiesPage.studenti.isPresent()).toBe(true);
        expect(entitiesPage.studenti.isDisplayed()).toBe(true);

        entitiesPage.studenti.click();

        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/studentis');

        studentPage.getEditButton('E1234').click();

        expect(createOrEditStudentPage.indeks.isPresent()).toBe(true);
        expect(createOrEditStudentPage.indeks.isDisplayed()).toBe(true);
        expect(createOrEditStudentPage.ime.isPresent()).toBe(true);
        expect(createOrEditStudentPage.ime.isDisplayed()).toBe(true);
        expect(createOrEditStudentPage.prezime.isPresent()).toBe(true);
        expect(createOrEditStudentPage.prezime.isDisplayed()).toBe(true);
        expect(createOrEditStudentPage.grad.isPresent()).toBe(true);
        expect(createOrEditStudentPage.grad.isDisplayed()).toBe(true);

        
        createOrEditStudentPage.indeks = 'E1234';
        createOrEditStudentPage.ime = 'Marko';
        createOrEditStudentPage.prezime = 'Markovic';
        createOrEditStudentPage.grad = 'Kraljevo';

        modalWindowPage.getSaveButton.click();

        var newStudent = studentPage.getStudentRow('E1234'); 

        expect(newStudent.isPresent()).toBe(true);
        expect(newStudent.isDisplayed()).toBe(true);
        expect(newStudent.getText()).toContain('Kraljevo');

        menuPage.accountMenu.click();
        menuPage.logOut.click();
    });
});        