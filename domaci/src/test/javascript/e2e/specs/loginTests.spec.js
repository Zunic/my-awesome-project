var LoginPage = require('../page_objects/login.page.js');
var MenuPage = require('../page_objects/menu.page.js');
var HomePage = require('../page_objects/home.page.js');

describe('Login page:', function() {
  var loginPage;
  var menuPage;
  var homePage;
    
  beforeAll(function() {
    // Pre svega navigiramo na stranicu koju testiramo
    browser.navigate().to("localhost:8080/#/");
    loginPage = new LoginPage();
    menuPage = new MenuPage();
    homePage = new HomePage();
  });

  it('should not log in with username "" and password "".', function(){

        menuPage.accountMenu.click();

        expect(menuPage.signIn.isPresent()).toBe(true);
        expect(menuPage.signIn.isDisplayed()).toBe(true);

        menuPage.signIn.click();

        expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/#/login');

        loginPage.login('', '');

        expect(loginPage.loginFailText).toContain('Failed to sign in!');
        

  })



});