var people = require('../model.js');
var _ = require('lodash');

var najstariji = 0;
var niz = [];

_.each(people, function(person) {
    if (person.age > najstariji) {
        najstariji = person.age;
    }
    return najstariji;
})

_.each(people, function(person) {
    if (person.age === najstariji) {
        niz.push(person.firstName + " " + person.lastName)
    }
})

console.log(niz)