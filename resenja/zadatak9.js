var people = require('../model.js');
var _ = require('lodash');

var niz = [];

var trcanje = function(person) {
    var counterRun = 0;
    for (i = 0; i < person.skills.length; i++) {
        if (person.skills[i] == "run") {
            counterRun++;
        }
    }
    return counterRun;
}

var skakanje = function(person) {
    var counterJump = 0;
    for (i = 0; i < person.skills.length; i++) {
        if (person.skills[i] == "jump") {
            counterJump++;
        }
    }
    return counterJump;
}

_.each(people, function(person) {
    if (trcanje(person) > 0 && skakanje(person) > 0) {
        niz.push (person.firstName + " " + person.lastName)
    }
})

console.log(niz)