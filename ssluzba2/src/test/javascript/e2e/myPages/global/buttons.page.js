var ButtonsPage = function() {};
var utils = require('../utils.js');

ButtonsPage.prototype = Object.create({}, {

    odustaniBtn: {

        get: function(){
            return utils.waitForElementPresence(by.buttonText(' Odustani'), 10000);
        }
    },

    sacuvajBtn: {

        get: function() {
            var buttons = element.all(by.xpath('//button [@name="btnSave"]')).filter(function(btn) {
                return btn.isDisplayed() || btn.isEnabled();
            });
            return buttons;
        }
    },

    confirmPassChangeBtn: {

        get: function(){
            return utils.waitForElementPresence(by.xpath('//button [@name="btnChange"]'), 10000);
        }
    },

});

module.exports = ButtonsPage;
