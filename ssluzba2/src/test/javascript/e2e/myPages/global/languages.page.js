var LanguagesPage = function() {};
var utils = require('../utils.js');

LanguagesPage.prototype = Object.create({}, {

    jeziciBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//div [@class="navbar-custom-menu"]/ul[1]'), 10000); 
        }
    },

    srpskiLat: {

        get: function() {
            jeziciBtn.click();
            return utils.waitForElementPresence(by.xpath('//a [@translate="BUTTON_LANG_SR_LAT"]'), 10000);
        }
    },    

    srpskiCir: {

        get: function() {
            jeziciBtn.click();
            return utils.waitForElementPresence(by.xpath('//a [@translate="BUTTON_LANG_SR_CYR"]'), 10000);
        }
    },    

    engleski: {

        get: function() {
            jeziciBtn.click();
            return utils.waitForElementPresence(by.xpath('//a [@translate="BUTTON_LANG_EN"]'), 10000);
        }
    },    

});

module.exports = LanguagesPage;