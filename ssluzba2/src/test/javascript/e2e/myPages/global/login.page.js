var LoginPage = function() {};
var utils = require('../utils.js');

LoginPage.prototype = Object.create({}, {

    username: {

        get: function() {
            return utils.waitForElementPresence(by.id('username'), 10000);
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },

    // usernameErrors: {

    //     get: function() {
    //         var errors =  element.all(by.css('//#id=username/../span[2]')).filter(function(err) {
    //             return err.isDisplayed();
    //         });

    //         return errors.getText();
    //     }
    // },

    
    password: {

        get: function() {
            return utils.waitForElementPresence(by.id('password'), 10000);
        },
        set: function(value) {
            this.password.clear();
            this.password.sendKeys(value);
        }
    },

    // passwordErrors: {

    //     get: function() {
    //         var errors = element.all(by.css('//#id=username/../span[2]')).filter(function(err) {
    //             return err.isDisplayed();
    //         });

    //         return errors.getText();
    //     }
    // },
    
    signInBtn: {

        get: function() {
            return utils.waitForElementPresence(by.buttonText('Prijavi se'), 10000);
        }
    },

    cancelBtn: {

        get: function() {
            return utils.waitForElementPresence(by.buttonText('Odustani'), 10000);
        }
    },

    forgotPassword: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//a [@ui-sref=\"forgotPassword\"]'));
        }
    },
    
    navigateToPage: {

        value: function() {
            browser.get('http://park.ftn.uns.ac.rs:8080/#/login');
            browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                    return url === 'http://park.ftn.uns.ac.rs:8080/#/login';
                });
            }, 5000)
        }
    },
    
    login: {

        value: function(usernameString, passwordString) {
            this.username = usernameString;
            this.password = passwordString;
            this.signInBtn.click();
        }
    },

    usernameError: {

        get: function() {
            return utils.waitForElementPresence(by.xpath("//span [text()='Korisničko ime obavezno']"), 10000).getText();
        }
    },

});

module.exports = LoginPage;