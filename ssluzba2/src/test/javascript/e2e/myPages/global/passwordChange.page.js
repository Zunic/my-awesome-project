var PasswordChangePage = function() {};
var utils = require('../utils.js');

PasswordChangePage.prototype = Object.create({}, {

    staraLozinka: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="oldPassword"]'), 10000);
        },
        set: function(value) {
            this.staraLozinka.clear();
            this.staraLozinka.sendKeys(value)
        }
    },

    novaLozinka: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="newPassword"]'), 10000);
        },
        set: function(value) {
            this.novaLozinka.clear();
            this.novaLozinka.sendKeys(value)
        }
    },

    ponoviteNovuLozinku: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="newPasswordRepeat"]'), 10000);
        },
        set: function(value) {
            this.ponoviteNovuLozinku.clear();
            this.ponoviteNovuLozinku.sendKeys(value)
        }
    },

    errors: {

        get: function() {
            var errors = element.all(by.repeater('v in validations')).filter(function(err) {
                return err.isDisplayed();
            });
            return errors.getText();
        }
    },
    
});

module.exports = PasswordChangePage;