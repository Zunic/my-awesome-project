var SideMenuPage = function() {};
var utils = require('../utils.js');

SideMenuPage.prototype = Object.create({}, {

    institucijaBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//*[@id="main-page"]/aside/section/ul/li[3]/a'), 10000);
        }
    },

    istrazivaciBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//*[@id="main-page"]/aside/section/ul/li[4]/a'), 10000);
        }
    }

});

module.exports = SideMenuPage;