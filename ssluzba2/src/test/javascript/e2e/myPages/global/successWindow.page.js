var SuccessWindowPage = function() {};
var utils = require('../utils.js');

SuccessWindowPage.prototype = Object.create({}, {

    successWindow: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('SUCCESS'), 10000);
        }        
    },

    pauseBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath("//span [@class='fa fa-play']"), 10000);
        }
    },

    playBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath("//span [@class='fa fa-pause']"), 10000);
        }
    },

    exitBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath("//span [@class='fa fa-times']"), 10000);
        }
    },

});

module.exports = SuccessWindowPage;