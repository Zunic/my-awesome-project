var TopMenuPage = function() {};
var utils = require('../utils.js');

TopMenuPage.prototype = Object.create({}, {

    logoMini: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//span [@class="logo-mini"]'), 10000);
        }
    },

    logoLarge: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//span [@class="logo-lg"]'), 10000);
        }
    },

    sidebarToggle: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//a [@class="sidebar-toggle"]'), 10000);
        }
    },

    jeziciBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//div [@class="navbar-custom-menu"]/ul[1]'), 10000); 
        }
    },

    userMenuBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//div [@class="navbar-custom-menu"]/ul[2]'), 10000); 
        }
    },   
});

module.exports = TopMenuPage;