var UserMenuPage = function() {};
var utils = require('../utils.js');

UserMenuPage.prototype = Object.create({}, {

    userMenuBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//div [@class="navbar-custom-menu"]/ul[2]'), 10000); 
        }
    },

    uputstvoZaKoriscenje: {

        get: function(){
            return utils.waitForElementPresence(by.xpath('//span [@translate="USER_MANUAL"]'), 10000); 
        }
    },

    kontaktirajteNas: {

        get: function(){
            return utils.waitForElementPresence(by.xpath('//span [@translate="USER_SUPPORT"]'), 10000); 
        }
    },

    promenaLozinke: {

        get: function(){
            return utils.waitForElementPresence(by.xpath('//span [@translate="CHANGE_PASSWORD"]'), 10000); 
        }
    },

    odjava: {

        get: function(){
            return utils.waitForElementPresence(by.xpath('//span [@translate="LOGOUT"]'), 10000); 
        }
    },

});

module.exports = UserMenuPage;