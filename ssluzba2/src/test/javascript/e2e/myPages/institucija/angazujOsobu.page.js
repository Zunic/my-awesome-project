var AngazujOsobuPage = function () {};
var utils = require('../utils.js');

AngazujOsobuPage.prototype = Object.create({}, {

    osoba: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@placeholder="Pretraži osobe"]'), 10000);
        },
        set: function(value) {
            this.osoba.clear();
            this.osoba.sendKeys(value);
        }
    }, 

            osobaError: {

                get: function() {
                    var errors = element.all(by.className('help-block ng-binding')).filter(function(err) {
                        return err.isDisplayed();
                    });
                    return errors.getText();
                }
            },

    zvanje: {

        get: function() {
            return utils.waitForElementPresence(by.model('connection.position'), 10000);
        }
    }, 
            
            odaberiZvanje: {
                // value 0 = Asistent,
                // value 1 = Profesor,
                // value 2 = Saradnik u nastavi,

                value: function(zvanjeValue) {
                    zvanje.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + zvanjeValue + '"]'));
                }
            },
         
    funkcija: {

        get: function() {
            return utils.waitForElementPresence(by.model('connection.function'), 10000);
        }
   },

            odaberiFunkciju: {
                // value 0 = Funkcija 1,
                // value 1 = Dekan,

                value: function(funkcijaValue) {
                    funkcija.click();
                        return uitls.waitForElementPresence(by.xpath('//option [@value="' + funkcijaValue + '"]'));
                }
            },

    procenatZaposlenosti: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="employmentPercentage"]'), 10000);
        },
        set: function(value) {
            this.procenatZaposlenosti.clear();
            this.procenatZaposlenosti.sendKeys(value);
        }
    }, 
        
    brojMeseci: {

        get: function() {
            return utils.waitForElementPresence(by.model('connection.maxMonthEngagement'), 10000);
        }
    }, 

            odaberiBrojMeseci: {
                // value 0 = 0,
                // value 1 = 1,
                // ...

                value: function(brojMeseciValue) {
                    brojMeseci.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + brojMeseciValue + '"]'));
                }
            },

    od: {

        get: function() {
            var dates = element.all(by.model('data.date'));
                return dates[3];
        },
        set: function(value) {
            this.datumOsnivanja.clear();
            this.datumOsnivanja.sendKeys(value);
        }

    }, 

    do: {

        get: function() {
            var dates = element.all(by.model('data.date'));
                return dates[4];
        },
        set: function(value) {
            this.datumOsnivanja.clear();
            this.datumOsnivanja.sendKeys(value);
        }

    }, 

});

module.exports = AngazujOsobuPage;