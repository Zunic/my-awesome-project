var InstitucijaIstrazivaciPage = function () {};
var utils = require('../utils.js');

InstitucijaIstrazivaciPage.prototype = Object.create({}, {

    angazujOsobuBtn: {

        get: function() {
            return utils.waitForElementPresence(by.buttonText(' Angažuj osobu'), 10000);
        }
    },

    istrazvaciTable: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//table'), 10000);
        }
    },

    getIstrazivacRowByName: {

        value: function(nameString) {
            return istrazvaciTable.element(by.cssContainingText(nameString));
        }
    },

    deleteIstrazivacByName: {

        value: function(nameString) {
            return getIstrazivacRowByName(nameString).element(by.xpath('//button'));
            //  [@class="btn btn-sm btn-danger"]
        }
    },

    prviBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Prvi'), 100000);
        }
    },

    prethodniBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Prethodni'), 100000);
        }
    },

    sledećiBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Sledeći'), 100000);
        }
    },

    poslednjiBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Poslednji'), 100000);
        }
    },

    nemaIstrazivacaMessage: {

        get: function() {
            var message = utils.waitForElementPresence(By.cssContainingText('Nema angažovanih osoba!'), 10000);
                return message.getText();
        }
    }

});

module.exports = InstitucijaIstrazivaciPage;