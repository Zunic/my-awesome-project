var InstitucijaMenuPage = function() {};
var utils = require('../utils.js');

InstitucijaMenuPage.prototype = Object.create({}, {

    osnovniPodaci: {

        get: function() {
            // return utils.waitForElementPresence(by.binding('Osnovni podaci'), 10000);
            return utils.waitForElementPresence(by.xpath('//ul [@class=\"nav nav-tabs\"]/li[1]/a'), 10000);

        }
    },

    podaciZaRegistar: {

        get: function() {
            // return utils.waitForElementPresence(by.binding('Podaci za ragistar'), 10000);
            return utils.waitForElementPresence(by.xpath('//ul [@class=\"nav nav-tabs\"]/li[2]/a'), 10000);
        }
    },

    podaciZaProjekte: {

        get: function() {
            // return utils.waitForElementPresence(by.binding('Podaci za projekte'), 10000);
            return utils.waitForElementPresence(by.xpath('//ul [@class=\"nav nav-tabs\"]/li[3]/a'), 10000);
        }
    },

    istrazivaci: {

        get: function() {
            // return utils.waitForElementPresence(by.binding('Istraživači'), 10000);
            return utils.waitForElementPresence(by.xpath('//ul [@class=\"nav nav-tabs\"]/li[4]/a'), 10000);
        }
    },
    
    dodajIstrazivaca: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//a [@ui-sref="addPerson"]'), 10000);
        }
    },


});

module.exports = InstitucijaMenuPage;