var OsnovniPodaciPage = function() {};
var utils = require('../utils.js');

OsnovniPodaciPage.prototype = Object.create({}, {

    nazivInstitucije: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="name"]'), 10000); 
        },
        set: function(value) {
            this.nazivInstitucije.clear();
            this.nazivInstitucije.sendKeys(value);
        }
    },

    nazivInstitucijeNaEngleskom: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="eng_name"]'), 10000);
        },
        set: function(value) {
            this.nazivInstitucijeNaEngleskom.clear();
            this.nazivInstitucijeNaEngleskom.sendKeys(value);
        }
    },

    drzava: {

        get: function() {
            var drzavaField = utils.waitForElementPresence(by.xpath('//input [@name="state"]'), 10000);
                return drzavaField;
        }
    },

            dodajNovuDrzavuOption: {

                get: function() {
                    drzavaField.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="-1"]'), 10000);
                }
            },

            odaberiDrzavu: {

                value: function(drzavaString) {
                    drzavaField.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + drzavaString + '"]'), 10000);
                }
            },

            nazivNoveDrzave: {

                get: function() {
                    return utils.waitForElementPresence(by.xpath('//input [@name="stateDescription"]'), 10000); 
                },
                set: function(value) {
                    this.nazivNoveDrzave.clear();
                    this.nazivNoveDrzave.sendKeys(value);
                }
            },
        
            opisNoveDrzave: {

                get: function() {
                    return utils.waitForElementPresence(by.xpath('//input [@name="stateName"]'), 10000); 
                },
                set: function(value) {
                    this.opisNoveDrzave.clear();
                    this.opisNoveDrzave.sendKeys(value);
                }
            },

            sacuvajDrzavuBtn: {

                get: function() {
                    return utils.waitForElementPresence(by.xpath('//button [@type="submit"]'), 10000);
                }
            },  
 
    mesto: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="place"]'), 10000);
        },
        set: function(value) {
            this.mesto.clear();
            this.mesto.sendKeys(value);
        }
    },

    opstina: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="townShipText"]'), 10000);
        },
        set: function(value) {
            this.opstina.clear();
            this.opstina.sendKeys(value);
        }
    },

    ulicaIBroj: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="address"]'), 10000);
        },
        set: function(value) {
            this.ulicaIBroj.clear();
            this.ulicaIBroj.sendKeys(value);
        }
    },

    vebAdresa: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="uri"]'), 10000);
        },
        set: function(value) {
            this.vebAdresa.clear();
            this.vebAdresa.sendKeys(value);
        }
    },

    elektronskaPosta: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="email"]'), 10000);
        },
        set: function(value) {
            this.elektronskaPosta.clear();
            this.elektronskaPosta.sendKeys(value);
        }
    },

    telefon: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="phone"]'), 10000);
        },
        set: function(value) {
            this.telefon.clear();
            this.telefon.sendKeys(value);
        }
    },

    skraceniNaziv: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="acro"]'), 10000);
        },
        set: function(value) {
            this.skraceniNaziv.clear();
            this.skraceniNaziv.sendKeys(value);
        }
    },

    errors: {

        get: function() {
            var errors = element.all(by.repeater('v in validations')).filter(function(err) {
                return err.isDisplayed();
            });
            return errors.getText();
        }
    }

});

module.exports = OsnovniPodaciPage;