var PodaciZaProjektePage = function() {};
var utils = require('../utils.js');

PodaciZaProjektePage.prototype = Object.create({}, {

    brojRacuna: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="account"]'), 10000);
        },
        set: function(value) {
            this.brojRacuna.clear();
            this.brojRacuna.sendKeys(value);
        }
    }, 

    idUMinistarstvu: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="mntrID"]'), 10000);
        }
    }, 

    idNaMedjNivou: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="orcid"]'), 10000);
        },
        set: function(value) {
            this.idNaMedjNivou.clear();
            this.idNaMedjNivou.sendKeys(value);
        }
    }, 

    statusInstitucije: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="institutionStatus"]'), 10000);
        },
        set: function(value) {
            this.statusInstitucije.clear();
            this.statusInstitucije.sendKeys(value);
        }
    }, 

    oblastIstrazivanja: {

        get: function() {
            return utils.waitForElementPresence(by.id('s2id_autogen37'), 10000);
        },
        set: function(value) {
            this.oblastIstrazivanja.clear();
            this.oblastIstrazivanja.sendKeys(value);
        }
    }, 

    errors: {

        get: function() {
            var errors = element.all(by.repeater('v in validations')).filter(function(err) {
                return err.isDisplayed();
            });
            return errors.getText();
        }
    },

});

module.exports = PodaciZaProjektePage;
