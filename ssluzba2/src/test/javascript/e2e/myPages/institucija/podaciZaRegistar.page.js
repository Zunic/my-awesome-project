var PodaciZaRegistarPage = function() {};
var utils = require('../utils.js');

PodaciZaRegistarPage.prototype = Object.create({}, {

    pib: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="pib"]'), 10000);
        },
        set: function(value) {
            this.pib.clear();
            this.pib.sendKeys(value);
        }
    }, 

    maticniBroj: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="maticniBroj"]'), 10000);
        },
        set: function(value) {
            this.maticniBroj.clear();
            this.maticniBroj.sendKeys(value);
        }      
    }, 

    brPoslednjeAkreditacije: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="accreditationNumber"]'), 10000);
        },
        set: function(value) {
            this.brPoslednjeAkreditacije.clear();
            this.brPoslednjeAkreditacije.sendKeys(value);
        }
    }, 

    datumPoslednjeAkreditacije: {

        get: function() {
            var dates = element.all(by.model('data.date'));
                return dates[1];
            } ,        
        set: function(value) {
            this.datumPoslednjeAkreditacije.clear();
            this.datumPoslednjeAkreditacije.sendKeys(value);
        }
    },


    nazivIzAkreditacije: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="accreditationNote"]'), 10000);
        },
        set: function(value) {
            this.nazivIzAkreditacije.clear();
            this.nazivIzAkreditacije.sendKeys(value);
        }
    }, 

    napomenaORegistru: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="note"]'), 10000);
        },
        set: function(value) {
            this.napomenaORegistru.clear();
            this.napomenaORegistru.sendKeys(value);
        }
    }, 

    vrstaInstitucije: {

        get: function() {
            return utils.waitForElementPresence(by.model('data.type'), 10000);
        },
        set: function(value) {
            this.vrstaInstitucije.clear();
            this.vrstaInstitucije.sendKeys(value);
        }
    }, 

    osnivac: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="founder"]'), 10000);
        },
        set: function(value) {
            this.osnivac.clear();
            this.osnivac.sendKeys(value);
        }
    }, 

    datumOsnivanja: {

        get: function() {
            var dates = element.all(by.model('data.date'));
                return dates[2];
        },
        set: function(value) {
            this.datumOsnivanja.clear();
            this.datumOsnivanja.sendKeys(value);
        }
    }, 

    brojResenja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="rescriptNumber"]'), 10000);
        },
        set: function(value) {
            this.brojResenja.clear();
            this.brojResenja.sendKeys(value);
        }
    }, 

            vlasnickaStruktura: {

                get: function() {
                    return utils.waitForElementPresence(by.xpath('//input [@name="ownershipStructure"]'), 10000);
                }
            }, 

            odaberiVlasnickuStrukturu: {
                // value 0 = Drzavna,
                // value 1 = Privatna,
                // value 2 = Mesovita,

                value: function(strukturaValue) {
                    vlasnickaStruktura.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + strukturaValue + '"]'));
                }
            },
            

    errors: {

        get: function() {
            var errors = element.all(by.repeater('v in validations')).filter(function(err) {
                return err.isDisplayed();
            });
            return errors.getText();
        }
    },

});

module.exports = PodaciZaRegistarPage;