var DodajIstrazivacaPage = function() {};
var utils = require('../utils.js');

DodajIstrazivacaPage.prototype = Object.create({}, {

    licniPodaci: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//ul [@class="nav nav-tabs"]/li[1]'), 10000); 
        }
    },

    podaciZaRegistar: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//ul [@class="nav nav-tabs"]/li[2]'), 10000); 
        }
    },

    podaciZaProjekte: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//ul [@class="nav nav-tabs"]/li[3]'), 10000); 
        }
    },

    ime: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="firstNameText"]'), 10000);
        },
        set: function(value) {
            this.ime.clear();
            this.ime.sendKeys(value);
        }
    },    

    prezime: {

        get: function() {
            return utils.waitForElementPresence(by.id('personSearchLastNameT'), 10000);
        },
        set: function(value) {
            this.prezime.clear();
            this.prezime.sendKeys(value);
        }
    },    
    imeJednogRoditelja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="middleName"]'), 10000);
        },
        set: function(value) {
            this.imeJednogRoditelja.clear();
            this.imeJednogRoditelja.sendKeys(value);
        }
    },    

    titulaIstrazivaca: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//select [@name="personTitle"]'), 10000);
        },
            odaberiTitulu: {
                // value dr = Dr,
                // value mr = Mr,

                value: function(titulaValue) {
                    titulaIstrazivaca.click();
                    return utils.waitForElementPresence(by.xpath('//option [@value="' + titulaValue + '"]'));
                }
            }
    },

    datumRodjenja: {

        get: function() {
            return utils.waitForElementPresence(by.model('data.date"]'), 10000);
        },
        set: function(value) {
            this.datumRodjenja.clear();
            this.datumRodjenja.sendKeys(value);
        }
    },    

    drzavaRodjenja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="state"]'), 10000);
        },
        set: function(value) {
            this.drzavaRodjenja.clear();
            this.drzavaRodjenja.sendKeys(value);
        }
    },    

    mestoRodjenja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="placeOfBirth"]'), 10000);
        },
        set: function(value) {
            this.mestoRodjenja.clear();
            this.mestoRodjenja.sendKeys(value);
        }
    },    

    opstinaRodjenja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="townShipOfBirth"]'), 10000);
        },
        set: function(value) {
            this.opstinaRodjenja.clear();
            this.opstinaRodjenja.sendKeys(value);
        }
    },    

    drzavaBoravista: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="stateOfResidence"]'), 10000);
        },
        set: function(value) {
            this.drzavaBoravista.clear();
            this.drzavaBoravista.sendKeys(value);
        }
    },    

    mestoBoravista: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="city"]'), 10000);
        },
        set: function(value) {
            this.mestoBoravista.clear();
            this.mestoBoravista.sendKeys(value);
        }
    },    

    opstinaBoravista: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="townShipOfResidence"]'), 10000);
        },
        set: function(value) {
            this.opstinaBoravista.clear();
            this.opstinaBoravista.sendKeys(value);
        }
    },    

    ulicaIBroj: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="address"]'), 10000);
        },
        set: function(value) {
            this.ulicaIBroj.clear();
            this.ulicaIBroj.sendKeys(value);
        }
    },    

    pol: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//select [@name="gender"]'), 10000);
        },
            odaberiPol: {
                // value 0 = Muski,
                // value 1 = Zenski,

                value: function(polValue) {
                    pol.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + polValue + '"]'));
                }
            }
    },

    jmbg: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="jmbg"]'), 10000);
        },
        set: function(value) {
            this.jmbg.clear();
            this.jmbg.sendKeys(value);
        }
    },    

    email: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="email"]'), 10000);
        },
        set: function(value) {
            this.email.clear();
            this.email.sendKeys(value);
        }
    },    

    telefon: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="phones"]'), 10000);
        },
        set: function(value) {
            this.telefon.clear();
            this.telefon.sendKeys(value);
        }
    },    

    licnaVebAdresa: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="uri"]'), 10000);
        },
        set: function(value) {
            this.licnaVebAdresa.clear();
            this.licnaVebAdresa.sendKeys(value);
        }
    },    

    statusIstrazivaca: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//select [@name="personStatus"]'), 10000);
        },
            odaberiStatus: {
                // value 0 = Status 1,
                // value 1 = Status 2,
                // value 2 = Status 3,

                value: function(statusValue) {
                    statusIstrazivaca.click();
                        return utils.waitForElementPresence(by.xpath('//option [@value="' + statusValue + '"]'));
                }
            }
    },
    

    errors: {

        get: function() {
            var errors = element.all(by.repeater('v in validations')).filter(function(err) {
                return err.isDisplayed();
            });
            return errors.getText();
        }
    },

});

module.exports = DodajIstrazivacaPage;
