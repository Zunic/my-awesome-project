var IstrazivaciPage = function() {};
var utils = require('../utils.js');

IstrazivaciPage.prototype = Object.create({}, {

    pretraziIstrazivace: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@placeholder="Pretraži istraživače"]'), 10000);
        },
        set: function(value) {
            this.pretraziIstrazivace.clear();
            this.pretraziIstrazivace.sendKeys(value);
        }
    }, 

    dodajIstrazivacaBtn: {

        get: function() {
            return utils.waitForElementPresence(by.buttonText(' Dodaj istraživača'), 10000);
        }
    },

    istrazvaciTable: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//table'), 10000);
        }
    },

    migriraniPodaci: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="migrated"]'), 10000);
        }
    },

    verifikovaniMigriraniPodaci: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="changed"]'), 10000);
        }
    },

    imeBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Ime'), 10000);
        }
    },

     prezimeBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Prezime'), 10000);
        }
    },

    datumRodjenjaBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Datum rođenja'), 10000);
        }
    },

    excelBtn: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//div [@ng-click="ctrl.exportExcel()"]'), 10000);
        }
    },
   

    prviBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Prvi'), 100000);
        }
    },

    prethodniBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Prethodni'), 100000);
        }
    },

    sledećiBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Sledeći'), 100000);
        }
    },

    poslednjiBtn: {

        get: function() {
            return utils.waitForElementPresence(by.cssContainingText('Poslednji'), 100000);
        }
    },

    nemaIstrazivacaMessage: {

        get: function() {
            var message = utils.waitForElementPresence(By.cssContainingText('Nema istraživača!'), 10000);
            return message.getText();
        }
    },   

});

module.exports = IstrazivaciPage;