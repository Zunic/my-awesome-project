var PodaciZaprojektePage = function() {};
var utils = require('../utils.js');

PodaciZaprojektePage.prototype = Object.create({}, {

    tipOsobe: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//select [@name="personType"]'), 10000);
        }
    },

    obracunskoZvanje: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//select [@name="personPosition"]'), 10000);
        }
    },

    kategorijeIstrazivaca: {

        get: function() {
            return utils.waitForElementPresence(by.id('s2id_autogen16"]'), 10000);
        }
    },
    

});

module.exports = PodaciZaprojektePage;