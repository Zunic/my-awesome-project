var PodaciZaRegistar = function() {};
var utils = require('../utils.js');

PodaciZaRegistar.prototype = Object.create({}, {

    bibliografija: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="bibliography"]'), 10000); 
        }
    },

    oblastiIstrazivanja: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="researchAreas"]'), 10000); 
        }
    },

    id: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="mntrn"]'), 10000); 
        }
    },

    napomena: {

        get: function() {
            return utils.waitForElementPresence(by.xpath('//input [@name="note"]'), 10000); 
        }
    },

});

module.exports = PodaciZaRegistar;