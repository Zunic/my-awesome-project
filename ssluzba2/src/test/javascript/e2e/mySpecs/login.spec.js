var LoginPage = require('../myPages/global/login.page.js');
var SideMenuPage = require('../myPages/global/sideMenu.page.js');
var InstitucijaMenuPage = require('../myPages/institucija/institucijaMenu.page.js');
var baseUrl = 'http://park.ftn.uns.ac.rs:8080/#/';

describe('Login page: ', function() {

    var loginPage;
    var sideMenuPage;
    var institucijaMenuPage;

    beforeAll(function() {

        browser.navigate().to("http://park.ftn.uns.ac.rs:8080/");
        loginPage = new LoginPage();
        sideMenuPage = new SideMenuPage();
        institucijaMenuPage = new InstitucijaMenuPage();
    });

    it('should not log in', function(){

        loginPage.username = 'ne';
        loginPage.username.clear();

        loginPage.password = 'z4ph0d818l8r0x';

        loginPage.signInBtn.click();

        expect(loginPage.usernameError).toContain('Korisničko ime obavezno');
    }),

    it('should successfully log in as Nemanja Zunic', function(){

        loginPage.username = 'nemanja.zunic1@gmail.com';

        loginPage.password = 'z4ph0d818l8r0x';

        loginPage.signInBtn.click();

        sideMenuPage.institucijaBtn.click();

        
    });

});