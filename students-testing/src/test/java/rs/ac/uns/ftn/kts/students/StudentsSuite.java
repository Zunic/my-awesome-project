package rs.ac.uns.ftn.kts.students;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import rs.ac.uns.ftn.kts.students.service.StudentServiceTest;
import rs.ac.uns.ftn.kts.students.web.controller.StudentControllerTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  StudentServiceTest.class,
  StudentControllerTest.class
})
public class StudentsSuite {

}
