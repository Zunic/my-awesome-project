package rs.ac.uns.ftn.kts.students.web.controller;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_CARD_NUMBER;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_COUNT;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_COUNT_STUDENT_COURSES;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_COUNT_WITH_LAST_NAME;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_FIRST_NAME;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.DB_LAST_NAME;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.NEW_CARD_NUMBER;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.NEW_FIRST_NAME;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.NEW_LAST_NAME;
import static rs.ac.uns.ftn.kts.students.constants.StudentConstants.PAGE_SIZE;

import java.nio.charset.Charset;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import rs.ac.uns.ftn.kts.students.StudentsApplication;
import rs.ac.uns.ftn.kts.students.TestUtil;
import rs.ac.uns.ftn.kts.students.constants.EnrollmentConstants;
import rs.ac.uns.ftn.kts.students.constants.ExamConstants;
import rs.ac.uns.ftn.kts.students.constants.StudentConstants;
import rs.ac.uns.ftn.kts.students.model.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StudentsApplication.class)
@WebIntegrationTest
@TestPropertySource(locations="classpath:test.properties")
public class StudentControllerTest {
	
	private static final String URL_PREFIX = "/api/students";
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @PostConstruct
    public void setup() {
    	this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void testGetAllStudents() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/all"))
	        .andExpect(status().isOk())
	        .andExpect(content().contentType(contentType))
	        .andExpect(jsonPath("$", hasSize(DB_COUNT)))
	        .andExpect(jsonPath("$.[*].id").value(hasItem(StudentConstants.DB_ID.intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DB_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DB_LAST_NAME)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DB_CARD_NUMBER)));
    }
    
    @Test
    public void testGetStudentsPage() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "?page=1&size=" + PAGE_SIZE))
    		.andExpect(status().isOk())
    		.andExpect(content().contentType(contentType))
    		.andExpect(jsonPath("$", hasSize(PAGE_SIZE)))
    		.andExpect(jsonPath("$.[*].id").value(hasItem(StudentConstants.DB_ID.intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DB_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DB_LAST_NAME)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DB_CARD_NUMBER)));	
    }
    
    @Test
    public void testGetStudent() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/" + StudentConstants.DB_ID))
    	.andExpect(status().isOk())
    	.andExpect(content().contentType(contentType))
    	.andExpect(jsonPath("$.id").value(StudentConstants.DB_ID.intValue()))
        .andExpect(jsonPath("$.firstName").value(DB_FIRST_NAME))
        .andExpect(jsonPath("$.lastName").value(DB_LAST_NAME))
        .andExpect(jsonPath("$.cardNumber").value(DB_CARD_NUMBER));
    }
    
    @Test
    @Transactional
    @Rollback(true)
    public void testSaveStudent() throws Exception {
    	Student student = new Student();
		student.setFirstName(NEW_FIRST_NAME);
		student.setLastName(NEW_LAST_NAME);
		student.setCardNumber(NEW_CARD_NUMBER);
    	
    	String json = TestUtil.json(student);
        this.mockMvc.perform(post(URL_PREFIX)
                .contentType(contentType)
                .content(json))
                .andExpect(status().isCreated());
    }
    
    @Test
    @Transactional
    @Rollback(true)
    public void testUpdateStudent() throws Exception {
    	Student student = new Student();
    	student.setId(StudentConstants.DB_ID);
		student.setFirstName(NEW_FIRST_NAME);
		student.setLastName(NEW_LAST_NAME);
		student.setCardNumber(NEW_CARD_NUMBER);
    	
    	String json = TestUtil.json(student);
        this.mockMvc.perform(put(URL_PREFIX)
                .contentType(contentType)
                .content(json))
                .andExpect(status().isOk());
    }
    
    @Test
    @Transactional
    @Rollback(true)
    public void testDeleteStudent() throws Exception { 	
        this.mockMvc.perform(delete(URL_PREFIX + "/" + StudentConstants.DB_ID))
                .andExpect(status().isOk());
    }
    
    @Test
    public void testGetStudentByCard() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/findCard?cardNumber=" + DB_CARD_NUMBER))
    	.andExpect(status().isOk())
    	.andExpect(content().contentType(contentType))
    	.andExpect(jsonPath("$.id").value(StudentConstants.DB_ID.intValue()))
        .andExpect(jsonPath("$.firstName").value(DB_FIRST_NAME))
        .andExpect(jsonPath("$.lastName").value(DB_LAST_NAME))
        .andExpect(jsonPath("$.cardNumber").value(DB_CARD_NUMBER));
    }
    
    @Test
    public void testGetStudentByLastName() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/findLastName?lastName=" + DB_LAST_NAME))
    	.andExpect(status().isOk())
    	.andExpect(content().contentType(contentType))
    	.andExpect(jsonPath("$", hasSize(DB_COUNT_WITH_LAST_NAME)));
    }
    
    @Test
    public void testGetStudentCourses() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/" + StudentConstants.DB_ID_REFERENCED + "/courses"))
    		.andExpect(status().isOk())
    		.andExpect(content().contentType(contentType))
    		.andExpect(jsonPath("$", hasSize(DB_COUNT_STUDENT_COURSES)))
    		.andExpect(jsonPath("$.[*].id").value(
    				hasItem(EnrollmentConstants.DB_ID.intValue())))
    		.andExpect(jsonPath("$.[*].startDate").value(
    				hasItem(EnrollmentConstants.DB_START_DATE.getTime())))
    		.andExpect(jsonPath("$.[*].endDate").value(
    				hasItem(EnrollmentConstants.DB_END_DATE.getTime())))
    		.andExpect(jsonPath("$.[*].course.id").value(
    				hasItem(EnrollmentConstants.DB_COURSE_ID.intValue())));
    }
    
    @Test 
    public void testGetStudentExams() throws Exception {
    	mockMvc.perform(get(URL_PREFIX + "/" + StudentConstants.DB_ID_REFERENCED + "/exams"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(contentType))
		.andExpect(jsonPath("$", hasSize(DB_COUNT_STUDENT_COURSES)))
		.andExpect(jsonPath("$.[*].id").value(
    				hasItem(ExamConstants.DB_ID.intValue())))
    	.andExpect(jsonPath("$.[*].date").value(
    			hasItem(ExamConstants.DB_DATE.getTime())))
    	.andExpect(jsonPath("$.[*].examPoints").value(
    			hasItem(ExamConstants.DB_EXAM_POINTS)))
    	.andExpect(jsonPath("$.[*].labPoints").value(
    			hasItem(ExamConstants.DB_LAB_POINTS)))
    	.andExpect(jsonPath("$.[*].course.id").value(
    			hasItem(ExamConstants.DB_COURSE_ID.intValue())))
    	.andExpect(jsonPath("$.[*].examPeriod.id").value(
    			hasItem(ExamConstants.DB_EXAM_PERIOD_ID.intValue())));
    }
}
