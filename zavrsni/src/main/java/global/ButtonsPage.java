package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class ButtonsPage {
	private WebDriver driver;

	public ButtonsPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getCancelBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Odustani']"), 10);
	}

	public WebElement getSaveBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()=' Sačuvaj']"), 10);
	}

	public WebElement getSignInBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prijavi se']"), 10);
	}

	public WebElement confirmPassChangeBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button [@name='btnChange']"), 10);
	}

}
