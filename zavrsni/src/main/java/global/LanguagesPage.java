package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class LanguagesPage {
	private WebDriver driver;

	public LanguagesPage(WebDriver driver) {
		this.driver = driver;
	}	

	public WebElement getJeziciBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//div [@class='navbar-custom-menu']/ul[1]"), 10);
	}

	public WebElement srpskiLat() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@translate='BUTTON_LANG_SR_LAT']"), 10);
	}

	public WebElement srpskiCir() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@translate='BUTTON_LANG_SR_CYR']"), 10);
	}

	public WebElement engleski() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@translate='BUTTON_LANG_EN']"), 10);
	}

}
