package global;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class LoginPage {
	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getUsername() {
		return Utils.waitForElementPresence(driver, By.id("username"), 10);
	}
	
	public void setUsername(String username) {
		WebElement usernameField = getUsername();
		usernameField.clear();
		usernameField.sendKeys(username);
	}

	public WebElement usernameError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Korisničko ime obavezno']"), 10);
	}
	
	public WebElement getPassword() {
		return Utils.waitForElementPresence(driver, By.id("password"), 10);
	}
	
	public void setPassword(String username) {
		WebElement passwordField = getPassword();
		passwordField.clear();
		passwordField.sendKeys(username);
	}
	
	public WebElement passwordError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Lozinka obavezna']"), 10);
	}
	
	public WebElement getSignInBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prijavi se']"), 10);
	}

	public WebElement getCancelBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Odustani']"), 10);
	}
	
	public WebElement forgotPassword() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@ui-sref='forgotPassword']"), 10);
	}
	
	public void login (String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
		this.getSignInBtn().click();
	}
	
	public WebElement problemSaLogovanjem() {
		return Utils.waitForElementPresence(driver, By.xpath("//li [text()='Pogrešno korisničko ime ili lozinka!']"), 10);
	}
	
//	public String getError() {
//		
//		String errorMessage = null;
//		List<WebElement> errors = driver.findElements(By.xpath("//span [@ng-repeat='v in validations']"));
//		
//		for (int i = 0; i < errors.size(); i++) {
//			if (errors.get(i).isDisplayed()) {
//				 errorMessage =  errors.get(i).getText();
//			} 
//		}
//		return errorMessage; 
//	}
	


	
}
