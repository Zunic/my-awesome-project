package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class PasswordChangePage {
	private WebDriver driver;

	public PasswordChangePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getStaraLozinka() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='oldPassword']"), 10);
	}
	
	public void setStaraLozinka (String staraLozinka) {
		WebElement staraLozinkaField = getStaraLozinka();
		staraLozinkaField.clear();
		staraLozinkaField.sendKeys(staraLozinka);
	}
	
	public WebElement staraLozinkaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti staru lozinku.']"), 10);
	}
	
	public WebElement getNovaLozinka() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='newPassword']"), 10);
	}
	
	public void setNovaLozinka (String novaLozinka) {
		WebElement novaLozinkaField = getNovaLozinka();
		novaLozinkaField.clear();
		novaLozinkaField.sendKeys(novaLozinka);
	}
	
	public WebElement novaLozinkaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti novu lozinku.']"), 10);
	}

	public WebElement getPonoviNovuLozinku() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='newPasswordRepeat']"), 10);
	}
	
	public void setPonoviNovuLozinku (String ponovljenaLozinka) {
		WebElement ponovljenaLozinkaField = getPonoviNovuLozinku();
		ponovljenaLozinkaField.clear();
		ponovljenaLozinkaField.sendKeys(ponovljenaLozinka);
	}
	
	public WebElement ponovljenaLozinkaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti ponovljenu lozinku.']"), 10);
	}


}
