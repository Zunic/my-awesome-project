package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class SideMenuPage {
	private WebDriver driver;

	public SideMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getInstitucijaBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='main-page']/aside/section/ul/li[3]/a"), 10);
	}

	public WebElement getIstrazivaciBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='main-page']/aside/section/ul/li[4]/a"), 10);
	}

}
