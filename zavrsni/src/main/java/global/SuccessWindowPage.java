package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import utils.Utils;

public class SuccessWindowPage {
	private WebDriver driver;
		

	public SuccessWindowPage(WebDriver driver) {
		this.driver = driver;
	}
	

	public WebElement getSuccessWindow() {
		return Utils.waitForElementPresence(driver, By.xpath("//h4 [text()='SUCCESS']"), 10);
	}
	
	public WebElement getPlayButton() {
		getSuccessWindow().click();
		return Utils.waitForElementPresence(driver, By.xpath("//span [@class='fa fa-play']"), 10);
	}
	
	public WebElement getPauseButton() {
		getSuccessWindow().click();
		return Utils.waitForElementPresence(driver, By.xpath("//span [@class='fa fa-pause']"), 10);
	}
	
	public WebElement getExitButton() {
		getSuccessWindow().click();
		return Utils.waitForElementPresence(driver, By.xpath("//span [@class='fa fa-times']"), 10);
	}
	
	
}
