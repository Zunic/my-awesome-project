package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class TopMenuPage {
	private WebDriver driver;

	public TopMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getLogoMini() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@class='logo-mini']"), 10);
	}

	public WebElement getLogoLarge() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@class='logo-lg']"), 10);
	}

	public WebElement getSidebarToggle() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@class='sidebar-toggle']"), 10);
	}

	public WebElement getJeziciBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//div [@class='navbar-custom-menu']/ul[1]"), 10);
	}

	public WebElement getUserMenuBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//div [@class='navbar-custom-menu']/ul[2]"), 10);
	}

}
