package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class UserMenuPage {
	private WebDriver driver;

	public UserMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getUserMenuBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//div [@class='navbar-custom-menu']/ul[2]"), 10);
	}
	
	public WebElement getUputstvoZaKoriscenje() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@translate='USER_MANUAL']"), 10);
	}
	
	public WebElement getKontaktirajteNas() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@translate='USER_SUPPORT']"), 10);
	}

	public WebElement getPromenaLozinke() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@translate='CHANGE_PASSWORD']"), 10);
	}
	
	public WebElement getOdjava() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [@translate='LOGOUT']"), 10);
	}

}
