package institucija;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class AngazujOsobuPage {
	private WebDriver driver;

	public AngazujOsobuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getOsoba() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@placeholder='Pretraži osobe']"), 10);
	}
	
	public void setOsoba(String osoba) {
		WebElement osobaField = getOsoba();
		osobaField.clear();
		osobaField.sendKeys(osoba);
		WebElement dropdown = Utils.waitForElementPresence(driver, By.xpath("//ul [@class='dropdown-menu ng-isolate-scope']"), 10);
		dropdown.findElement(By.xpath("//li [@ng-repeat='match in matches track by $index'][1]")).click();
	}
	
	public WebElement osobaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Search query is too short']"), 10);
	}
	
	public WebElement noOsobaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='0 results found']"), 10);
	}
	
	

	public WebElement getZvanje() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='connection.position']"), 10);
	}
	
	public Select selectZvanje() {
		return new Select(Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='connection.position']"), 10));
	}
	
	public void odaberiZvanje(String zvanje) {
		this.selectZvanje().selectByVisibleText(zvanje);
	}
	
	
	
	public WebElement getFunkcija() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='connection.function']"), 10);
	}
	
	public Select selectFunkcija() {
		return new Select(Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='connection.function']"), 10));
	}
	
	public void odaberiFunkciju(String funkcija) {
		this.selectFunkcija().selectByVisibleText(funkcija);
	}
	
	

	public WebElement getProcenatZaposlenosti() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='employmentPercentage']"), 10);
	}
	
	public void setProcenatZaposlenosti(String procenat) {
		WebElement procenatField = getProcenatZaposlenosti();
		procenatField.clear();
		procenatField.sendKeys(procenat);
	}

	public WebElement procenatError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Uneta vrednost mora biti između 0 i 100']"), 10);
	}
	
	
	
	public WebElement getBrojMeseci() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='cconnection.maxMonthEngagement']"), 10);
	}
	
	public Select selectBrojMeseci() {
		return new Select(Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='cconnection.maxMonthEngagement']"), 10));
	}
		
	public void odaberiBrojMeseci(String brojMeseci) {
		this.selectBrojMeseci().selectByVisibleText(brojMeseci);
	}
	
	

	public WebElement getOd() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@name='startDate']//input"), 10);
	}
	
	public void setOd(String datumOd) {
		WebElement odField = getOd();
		odField.clear();
		odField.sendKeys(datumOd);
	}
	
	

	public WebElement getDo() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@name='endDate']//input"), 10);
	}
	
	public void setDo(String datumDo) {
		WebElement doField = getDo();
		doField.clear();
		doField.sendKeys(datumDo);
	}
	
	
	
	public void angazujOsobu(String osoba, String zvanje, String datumOd, String datumDo) {
		this.setOsoba(osoba);
		this.odaberiZvanje(zvanje);
		this.setOd(datumOd);
		this.setDo(datumDo);
	}
	
}
