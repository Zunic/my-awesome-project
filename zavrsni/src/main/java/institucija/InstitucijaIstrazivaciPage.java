package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bsh.util.Util;
import utils.Utils;

public class InstitucijaIstrazivaciPage {
	private WebDriver driver;

	public InstitucijaIstrazivaciPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement angazujOsobu() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()=' Angažuj osobu']"), 10);
	}
	
	public WebElement pretraziIstrazivace() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@placeholder='Pretraži istraživače']"), 10);
	}
	
	public WebElement searchToShortError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Search query is too short']"), 10);
	}
	
	public WebElement istrazivaciTable() {
		return Utils.waitForElementPresence(driver, By.xpath("//table"), 10);
	}
	
	public WebElement getIstrazivacRowByName(String name) {
		return istrazivaciTable().findElement
				(By.xpath("//td[contains(text(), '" + name + "')]/.."));
	}
	
	public void deleteIstrazivacByName(String name) {
		this.getIstrazivacRowByName(name).findElement(By.xpath("//button [@class='btn btn-sm btn-danger']")).click();;
	}
	
	public WebElement prviBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prvi']"), 10);
	}

	public WebElement prethodniBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prethodni']"), 10);
	}

	public WebElement sledeciBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Sledeći']"), 10);
	}

	public WebElement poslednjiBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Poslednji']"), 10);
	}
	
	public String nemaIstrazivacaMessage() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Nema angažovanih osoba!']"), 10).getText();
	}

}
