package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class InstitucijaMenuPage {
	private WebDriver driver;

	public InstitucijaMenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement osnovniPodaci() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[1]/a"), 10);
	}
	
	public WebElement podaciZaRegistar() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[2]/a"), 10);
	}
	
	public WebElement podaciZaProjekte() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[3]/a"), 10);
	}

	public WebElement istrazivaci() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[4]/a"), 10);
	}

	public WebElement dodajIstrazivaca() {
		return Utils.waitForElementPresence(driver, By.xpath("//a [@ui-sref='addPerson']"), 10);
	}	

}
