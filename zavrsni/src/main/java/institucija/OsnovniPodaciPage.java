package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.KeyDownAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bsh.util.Util;
import utils.Utils;

public class OsnovniPodaciPage {
	private WebDriver driver;

	public OsnovniPodaciPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNazivInstitucije() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='name']"), 10);
	}
	
	public void setNazivInstitucije(String nazivInstitucije) {
		WebElement nazivInstitucijeField = getNazivInstitucije();
		nazivInstitucijeField.clear();
		nazivInstitucijeField.sendKeys(nazivInstitucije);
	}

	public WebElement nazivError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti naziv.']"), 10);
	}
	
	public WebElement getnazivInstitucijeNaEngleskom() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='eng_name']"), 10);
	}
	
	public void setNazivInstitucijeNaEngleskom(String nazivInstitucijeNaEngleskom) {
		WebElement nazivInstitucijeNaEngleskomField = getnazivInstitucijeNaEngleskom();
		nazivInstitucijeNaEngleskomField.clear();
		nazivInstitucijeNaEngleskomField.sendKeys(nazivInstitucijeNaEngleskom);
	}

	public WebElement getDrzava() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@name='state']"), 10);
	}

	public Select selectDrzava() {
		return new Select(Utils.waitForElementPresence(driver, By.xpath("//select [@name='state']"), 10));
	}

			public void dodajNovuDrzavu() {
				this.selectDrzava().selectByVisibleText("Dodaj novu...");
			}
			
			public void odaberiDrzavu(String drzava) {
				this.selectDrzava().selectByVisibleText("drzava");
			}
		
			public WebElement getNazivNoveDrzave() {
				this.dodajNovuDrzavu();
				return driver.findElement(By.xpath("//input [@name='stateName']"));
			}
			
			public void setNazivNoveDrzave(String nazivNoveDrzave) {
				WebElement nazivNoveDrzaveField = getNazivNoveDrzave();
				nazivNoveDrzaveField.clear();
				nazivNoveDrzaveField.sendKeys(nazivNoveDrzave);
			}
		
			public WebElement getOpisNoveDrzave() {
//				dodajNovuDrzavu();
				return driver.findElement(By.xpath("//input [@name='stateDescription']"));
			}
			
			public void setOpisNoveDrzave(String opisNoveDrzave) {
				WebElement opisNoveDrzaveField = getOpisNoveDrzave();
				opisNoveDrzaveField.clear();
				opisNoveDrzaveField.sendKeys(opisNoveDrzave);
			}
			
			public WebElement sacuvajNovuDrzavu() {
				return Utils.waitForElementPresence(driver, By.xpath("//button [@type='submit']"), 10);
			}

	public WebElement getMesto() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='place']"), 10);
	}
	
	public void setMesto(String mesto) {
		WebElement mestoField = getMesto();
		mestoField.clear();
		mestoField.sendKeys(mesto);
	}

	public WebElement mestoError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti mesto.']"), 10);
	}

	public WebElement getOpstina() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='townShipText']"), 10);
	}
	
	public void setOpstina(String opstina) {
		WebElement opstinaField = getOpstina();
		opstinaField.clear();
		opstinaField.sendKeys(opstina);
	}
	
	public WebElement getAdresa() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='address']"), 10);
	}
	
	public void setAdresa(String adresa) {
		WebElement adresaField = getAdresa();
		adresaField.clear();
		adresaField.sendKeys(adresa);
	}
	
	public WebElement adresaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti adresu.']"), 10);
	}
	
	public WebElement getWebAdresa() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='uri']"), 10);
	}
	
	public void setWebAdresa(String webAdresa) {
		WebElement webAdresaField = getWebAdresa();
		webAdresaField.clear();
		webAdresaField.sendKeys(webAdresa);
	}

	public WebElement webAdresaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti veb adresu.']"), 10);
	}

	public WebElement getEmail() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='email']"), 10);
	}
	
	public void setEmail(String email) {
		WebElement emailField = getEmail();
		emailField.clear();
		emailField.sendKeys(email);
	}

	public WebElement emailError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti adresu elektronske pošte.']"), 10);
	}
	
	public WebElement getTelefon() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='phone']"), 10);
	}
	
	public void setTelefon(String telefon) {
		WebElement telefonField = getTelefon();
		telefonField.clear();
		telefonField.sendKeys(telefon);
	}

	public WebElement telefonError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti broj telefona.']"), 10);
	}

	public WebElement getSkraceniNaziv() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='acro']"), 10);
	}
	
	public void setSkraceniNaziv(String skraceniNaziv) {
		WebElement skraceniNazivField = getSkraceniNaziv();
		skraceniNazivField.clear();
		skraceniNazivField.sendKeys(skraceniNaziv);
	}
	
	public void unesiOsnovnePodatke(String nazivInstitucije, String nazivInstitucijeNaEngleskom, 
			String drzava, String mesto, String opstina,String adresa, String webAdresa, 
			String email, String telefon, String skraceniNaziv) {
		this.setNazivInstitucije(nazivInstitucije);
		this.setNazivInstitucijeNaEngleskom(nazivInstitucijeNaEngleskom);
		this.odaberiDrzavu(drzava);
		this.setMesto(mesto);
		this.setOpstina(opstina);
		this.setAdresa(adresa);
		this.setWebAdresa(webAdresa);
		this.setEmail(email);
		this.setTelefon(telefon);
		this.setSkraceniNaziv(skraceniNaziv);
			
	}

	public void unesiOsnovnePodatkeSaNovomDrzavom(String nazivInstitucije, String nazivInstitucijeNaEngleskom, 
			String nazivNoveDrzave, String opisNoveDrzave, String mesto, String opstina,String adresa, String webAdresa, 
			String email, String telefon, String skraceniNaziv) {
		this.setNazivInstitucije(nazivInstitucije);
		this.setNazivInstitucijeNaEngleskom(nazivInstitucijeNaEngleskom);
		this.setNazivNoveDrzave(nazivNoveDrzave);
		this.setOpisNoveDrzave(opisNoveDrzave);
		this.sacuvajNovuDrzavu().click();
		this.setMesto(mesto);
		this.setOpstina(opstina);
		this.setAdresa(adresa);
		this.setWebAdresa(webAdresa);
		this.setEmail(email);
		this.setTelefon(telefon);
		this.setSkraceniNaziv(skraceniNaziv);
			
	}

		
}
