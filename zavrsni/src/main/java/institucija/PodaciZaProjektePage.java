package institucija;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class PodaciZaProjektePage {
	private WebDriver driver;

	public PodaciZaProjektePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBrojRacuna() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='account']"), 10);
	}
	
	public void setBrojRacuna(String brojRacuna) {
		WebElement brojRacunaField = getBrojRacuna();
		brojRacunaField.clear();
		brojRacunaField.sendKeys(brojRacuna);
	}
	
	public WebElement brojRacunaEmptyError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti broj računa.']"), 10);
	}

	public WebElement brojRacunaWrongError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Broj računa mora biti u formatu XXX-XXXXXXXXXXXXX-XX.']"), 10);
	}

	public WebElement getIdUMinistarstvu() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='mntrID']"), 10);
	}

	public WebElement getIdNaMedjNivou() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='account']"), 10);
	}
	
	public void setIdNaMedjNivou(String idNaMedjNivou) {
		WebElement idNaMedjNivouField = getIdNaMedjNivou();
		idNaMedjNivouField.clear();
		idNaMedjNivouField.sendKeys(idNaMedjNivou);
	}

	public WebElement getStatusInstitucije() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='institutionStatus']"), 10);
	}
	
	public void setStatusInstitucije(String statusInstitucije) {
		WebElement statusInstitucijeField = getStatusInstitucije();
		statusInstitucijeField.clear();
		statusInstitucijeField.sendKeys(statusInstitucije);
	}

	public WebElement getOblastIstrazivanja() {
		return Utils.waitForElementPresence(driver, By.id("s2id_autogen37"), 10);
	}
	
	public void setOblastIstrazivanja(String oblastIstrazivanja) {
		WebElement oblastIstrazivanjaField = getOblastIstrazivanja();
		oblastIstrazivanjaField.clear();
		oblastIstrazivanjaField.sendKeys(oblastIstrazivanja);
	}

	
}
