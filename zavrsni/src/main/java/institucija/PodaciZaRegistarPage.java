package institucija;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class PodaciZaRegistarPage {
	private WebDriver driver;

	public PodaciZaRegistarPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getPib() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='pib']"), 10);
	}
	
	public void setPib(String pib) {
		WebElement pibField = getPib();
		pibField.clear();
		pibField.sendKeys(pib);
	}

	public WebElement pibError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti PIB.']"), 10);
	}

	public WebElement getMaticniBroj() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='maticniBroj']"), 10);
	}
	
	public void setMaticniBroj(String maticniBroj) {
		WebElement maticniBrojField = getMaticniBroj();
		maticniBrojField.clear();
		maticniBrojField.sendKeys(maticniBroj);
	}

	public WebElement maticniBrojError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Unesite matični broj.']"), 10);
	}

	public WebElement getBrojPoslednjeAkreditacije() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='accreditationNumber']"), 10);
	}
	
	public void setBrojPoslednjeAkreditacije(String brojPoslednjeAkreditacije) {
		WebElement brojPoslednjeAkreditacijeField = getBrojPoslednjeAkreditacije();
		brojPoslednjeAkreditacijeField.clear();
		brojPoslednjeAkreditacijeField.sendKeys(brojPoslednjeAkreditacije);
	}

	public WebElement brojPoslednjeAkreditacijeError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti broj akreditacije.']"), 10);
	}

	public WebElement getDatumPoslednjeAkreditacije() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@name='accreditationDate']//input"), 10);
	}
	
	public void setDatumPoslednjeAkreditacije(String datumPoslednjeAkreditacije) {
		WebElement datumPoslednjeAkreditacijeField = getDatumPoslednjeAkreditacije();
		datumPoslednjeAkreditacijeField.clear();
		datumPoslednjeAkreditacijeField.sendKeys(datumPoslednjeAkreditacije);
	}

	public WebElement getNazivIzAkreditacije() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='accreditationNote']"), 10);
	}
	
	public void setNazivIzAkreditacije(String nazivIzAkreditacije) {
		WebElement nazivIzAkreditacijeField = getNazivIzAkreditacije();
		nazivIzAkreditacijeField.clear();
		nazivIzAkreditacijeField.sendKeys(nazivIzAkreditacije);
	}
	
	public WebElement nazivIzAkreditacijeError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Morate uneti naziv institucije iz akreditacije.']"), 10);
	}

	public WebElement getNapomenaORegistru() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='note']"), 10);
	}
	
	public void setNapomenaORegistru(String napomenaORegistru) {
		WebElement napomenaORegistruField = getNapomenaORegistru();
		napomenaORegistruField.clear();
		napomenaORegistruField.sendKeys(napomenaORegistru);
	}

	public WebElement getVrstaInstitucije() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@ng-model='data.type']"), 10);
	}
	
	public void setVrstaInstitucije(String vrstaInstitucije) {
		WebElement vrstaInstitucijeField = getVrstaInstitucije();
		vrstaInstitucijeField.clear();
		vrstaInstitucijeField.sendKeys(vrstaInstitucije);
	}

	public WebElement getOsnivac() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='founder']"), 10);
	}
	
	public void getOsnivac(String osnivac) {
		WebElement osnivacField = getOsnivac();
		osnivacField.clear();
		osnivacField.sendKeys(osnivac);
	}

	public WebElement getDatumOsnivanja() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@name='date']//input"), 10);
	}
	
	public void setDatumOsnivanja(String datumOsnivanja) {
		WebElement datumOsnivanjaField = getDatumOsnivanja();
		datumOsnivanjaField.clear();
		datumOsnivanjaField.sendKeys(datumOsnivanja);
	}

	public WebElement getBrojResenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='rescriptNumber']"), 10);
	}
	
	public void setBrojResenja(String brojResenja) {
		WebElement brojResenjaField = getBrojResenja();
		brojResenjaField.clear();
		brojResenjaField.sendKeys(brojResenja);
	}
	
	public WebElement getVlasnickaStruktura() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@ng-model='ownershipStructure']"), 10);
	}
	
	public WebElement odaberiVlasnickuStrukturu(String vlasnickaStruktura) {
		getVlasnickaStruktura().click();
		return Utils.waitForElementPresence(driver, By.xpath("//option [@value='" + vlasnickaStruktura + "']"), 10);
	}


}
