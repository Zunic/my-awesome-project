package istrazivaci;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class DodajIstrazivacaPage {
	private WebDriver driver;

	public DodajIstrazivacaPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getLicniPodaci() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[1]"), 10);
	}
	
	public WebElement getPodaciZaRegistar() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[2]"), 10);
	}

	public WebElement getPodaciZaProjekte() {
		return Utils.waitForElementPresence(driver, By.xpath("//ul [@class='nav nav-tabs']/li[3]"), 10);
	}

	public WebElement getIme() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='firstNameText']"), 10);
	}
	
	public void setIme(String ime) {
		WebElement imeField = getIme();
		imeField.clear();
		imeField.sendKeys(ime);
	}
	
	public WebElement osobaError() {
		return Utils.waitForElementPresence(driver, By.xpath("//span [text()='Search query is too short']"), 10);
	}
	
	public WebElement getPrezime() {
		return Utils.waitForElementPresence(driver, By.id("personSearchLastNameT"), 10);
	}
	
	public void setPrezime(String prezime) {
		WebElement prezimeField = getPrezime();
		prezimeField.clear();
		prezimeField.sendKeys(prezime);
	}

	public WebElement getImeJednogRoditelja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='middleName']"), 10);
	}
	
	public void setImeJednogRoditelja(String imeJednogRoditelja) {
		WebElement imeJednogRoditeljaField = getImeJednogRoditelja();
		imeJednogRoditeljaField.clear();
		imeJednogRoditeljaField.sendKeys(imeJednogRoditelja);
	}

	public WebElement getTitulaIstrazivaca() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='personTitle']"), 10);
	}
	
	public WebElement odaberiTituluIstrazivaca(String titulaIstrazivaca) {
		getTitulaIstrazivaca().click();
		return Utils.waitForElementPresence(driver, By.xpath("//option [@value='" + titulaIstrazivaca + "']"), 10);
	}

	public WebElement getDatumRodjenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@ng-model='data.date']"), 10);
	}
	
	public void setDatumRodjenja(String datumRodjenja) {
		WebElement datumRodjenjaField = getDatumRodjenja();
		datumRodjenjaField.clear();
		datumRodjenjaField.sendKeys(datumRodjenja);
	}

	public WebElement getDrzavaRodjenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='state']"), 10);
	}
	
	public void setDrzavaRodjenja(String drzavaRodjenja) {
		WebElement drzavaRodjenjaField = getDrzavaRodjenja();
		drzavaRodjenjaField.clear();
		drzavaRodjenjaField.sendKeys(drzavaRodjenja);
	}

	public WebElement getMestoRodjenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='placeOfBirth']"), 10);
	}
	
	public void setMestoRodjenja(String mestoRodjenja) {
		WebElement mestoRodjenjaField = getMestoRodjenja();
		mestoRodjenjaField.clear();
		mestoRodjenjaField.sendKeys(mestoRodjenja);
	}
	
	public WebElement getOpstinaRodjenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='townShipOfBirth']"), 10);
	}
	
	public void setOpstinaRodjenja(String opstinaRodjenja) {
		WebElement opstinaRodjenjaField = getOpstinaRodjenja();
		opstinaRodjenjaField.clear();
		opstinaRodjenjaField.sendKeys(opstinaRodjenja);
	}
	
	public WebElement getDrzavaBoravista() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='stateOfResidence']"), 10);
	}
	
	public void setDrzavaBoravista(String drzavaBoravista) {
		WebElement drzavaBoravistaField = getDrzavaBoravista();
		drzavaBoravistaField.clear();
		drzavaBoravistaField.sendKeys(drzavaBoravista);
	}

	public WebElement getMestoBoravista() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='city']"), 10);
	}
	
	public void setMestoBoravista(String mestoBoravista) {
		WebElement mestoBoravistaField = getMestoBoravista();
		mestoBoravistaField.clear();
		mestoBoravistaField.sendKeys(mestoBoravista);
	}

	public WebElement getOpstinaBoravista() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='townShipOfResidence']"), 10);
	}
	
	public void gstOpstinaBoravista(String opstinaBoravista) {
		WebElement opstinaBoravistaField = getOpstinaBoravista();
		opstinaBoravistaField.clear();
		opstinaBoravistaField.sendKeys(opstinaBoravista);
	}

	public WebElement getAdresa() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='address']"), 10);
	}
	
	public void setAdresa(String adresa) {
		WebElement adresaField = getAdresa();
		adresaField.clear();
		adresaField.sendKeys(adresa);
	}

	public WebElement getPol() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='personTitle']"), 10);
	}
	
	public WebElement odaberiPol(String gender) {
		getPol().click();
		return Utils.waitForElementPresence(driver, By.xpath("//option [@value='" + gender + "']"), 10);
	}

	public WebElement getJmbg() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='jmbg']"), 10);
	}
	
	public void setJmbg(String jmbg) {
		WebElement jmbgField = getJmbg();
		jmbgField.clear();
		jmbgField.sendKeys(jmbg);
	}

	public WebElement getEmail() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='email']"), 10);
	}
	
	public void setEmail(String email) {
		WebElement emailField = getEmail();
		emailField.clear();
		emailField.sendKeys(email);
	}

	public WebElement getTelefon() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='phones']"), 10);
	}
	
	public void setTelefon(String telefon) {
		WebElement telefonField = getTelefon();
		telefonField.clear();
		telefonField.sendKeys(telefon);
	}

	public WebElement getLicnaWebAdresa() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='uri']"), 10);
	}
	
	public void setLicnaWebAdresa(String licnaWebAdresa) {
		WebElement licnaWebAdresaField = getLicnaWebAdresa();
		licnaWebAdresaField.clear();
		licnaWebAdresaField.sendKeys(licnaWebAdresa);
	}
	
	public WebElement getStatusIstrazivaca() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='personStatus']"), 10);
	}
	
	public WebElement odaberiStatusIstrazivaca(String status) {
		getStatusIstrazivaca().click();
		return Utils.waitForElementPresence(driver, By.xpath("//option [@value='" + status + "']"), 10);
	}


}
