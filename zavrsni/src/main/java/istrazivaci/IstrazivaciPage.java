package istrazivaci;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class IstrazivaciPage {
	private WebDriver driver;

	public IstrazivaciPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getPretraziIstrazivace() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@placeholder='Pretraži istraživače']"), 10);
	}
	
	public void setPretraziIstrazivace(String pretraziIstrazivace) {
		WebElement pretraziIstrazivaceField = getPretraziIstrazivace();
		pretraziIstrazivaceField.clear();
		pretraziIstrazivaceField.sendKeys(pretraziIstrazivace);
	}

	public WebElement getDodajIstrazivacaBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()=' Dodaj istraživača']"), 10);
	}
	
	public WebElement getIstrazivaciTable() {
		return Utils.waitForElementPresence(driver, By.xpath("//table']"), 10);
	}

	public WebElement getMigriraniPodaci() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='migrated']"), 10);
	}

	public WebElement getVerifikovaniMigriraniPodaci() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='changed']"), 10);
	}

	public WebElement getImeBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Ime']"), 10);
	}

	public WebElement getPrezimeBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prezime']"), 10);
	}

	public WebElement getDatumRodjenjaBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Datum rođenja']"), 10);
	}

	public WebElement getExcelBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//div [@ng-click='ctrl.exportExcel()']"), 10);
	}
	
	public WebElement getPrviBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prvi']"), 10);
	}

	public WebElement getPrethodniBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Prethodni']"), 10);
	}

	public WebElement getSledeciBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Sledeći']"), 10);
	}

	public WebElement getPoslednjiBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Poslednji']"), 10);
	}

	public String getNemaIstrazivacaMessage() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Nema istraživača!']"), 10).getText();
	}

}
