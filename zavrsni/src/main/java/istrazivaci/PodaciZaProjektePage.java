package istrazivaci;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class PodaciZaProjektePage {
	private WebDriver driver;

	public PodaciZaProjektePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getTipOsobe() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@name='personType']"), 10);
	}

	public WebElement getObracunskoZvanje() {
		return Utils.waitForElementPresence(driver, By.xpath("//select [@name='personPosition']"), 10);
	}

	public WebElement getKategorijeIstrazivaca() {
		return Utils.waitForElementPresence(driver, By.id("s2id_autogen16"), 10);
	}

}
