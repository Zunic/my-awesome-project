package istrazivaci;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Utils;

public class PodaciZaRegistarPage {
	private WebDriver driver;

	public PodaciZaRegistarPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBibliografija() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='bibliography']"), 10);
	}

	public WebElement getOblastiIstrazivanja() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='researchAreas']"), 10);
	}

	public WebElement getId() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='mntrn']"), 10);
	}
	
	public WebElement getNapomena() {
		return Utils.waitForElementPresence(driver, By.xpath("//input [@name='note']"),10);
	}
	
}
