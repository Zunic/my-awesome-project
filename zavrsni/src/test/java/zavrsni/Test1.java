package zavrsni;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertEquals;

import global.ButtonsPage;
import global.LanguagesPage;
import global.LoginPage;
import global.SideMenuPage;
import global.TopMenuPage;
import global.UserMenuPage;
import institucija.AngazujOsobuPage;
import institucija.InstitucijaIstrazivaciPage;
import institucija.InstitucijaMenuPage;
import institucija.OsnovniPodaciPage;
import institucija.PodaciZaProjektePage;
import institucija.PodaciZaRegistarPage;

public class Test1 {

	private WebDriver driver;
	private LoginPage loginPage;
	private SideMenuPage sideMenuPage;
	private TopMenuPage topMenuPage;
	private LanguagesPage languagesPage;
	private UserMenuPage userMenuPage;
	private InstitucijaMenuPage institucijaMenuPage;
	private OsnovniPodaciPage osnovniPodaciPage;
	private ButtonsPage buttonsPage;
	private PodaciZaProjektePage podaciZaProjektePage;
	private PodaciZaRegistarPage podaciZaRegistarPage;
	private AngazujOsobuPage angazujOsobuPage;
	private InstitucijaIstrazivaciPage institucijaIstrazivaciPage;
	
	
	@BeforeSuite
	public void setUp() {
//		System.setProperty("webdriver.chrome.driver", "chromedriver");
//		driver = new ChromeDriver();
		driver = new FirefoxDriver();
//		String baseUrl = "http://192.168.58.25:8080/";
		String baseUrl = "http://park.ftn.uns.ac.rs:8080/";
		driver.manage().window().maximize();
		driver.get(baseUrl);
		loginPage = new LoginPage(driver);
		sideMenuPage = new SideMenuPage(driver);
		topMenuPage = new TopMenuPage(driver);
		languagesPage = new LanguagesPage(driver);
		userMenuPage = new UserMenuPage(driver);
		institucijaMenuPage = new InstitucijaMenuPage(driver);
		osnovniPodaciPage = new OsnovniPodaciPage(driver);
		buttonsPage = new ButtonsPage(driver);
		podaciZaProjektePage = new PodaciZaProjektePage(driver);
		podaciZaRegistarPage = new PodaciZaRegistarPage(driver);
		angazujOsobuPage = new AngazujOsobuPage(driver);
		institucijaIstrazivaciPage = new InstitucijaIstrazivaciPage(driver);
		loginPage.login("nemanja.zunic1@gmail.com", "z4ph0d818l8r0x");
	}
		
	@Test
	public void loginTest() throws InterruptedException {
		
//		global 
		
//		assertTrue(loginPage.getUsername().isDisplayed());
//		assertTrue(loginPage.getPassword().isDisplayed());
//		assertTrue(loginPage.getSignInBtn().isDisplayed());
//		
//		loginPage.setUsername("aa");
//		loginPage.getUsername().clear();
//		loginPage.setPassword("aa");
//		loginPage.getPassword().clear();
//		loginPage.login("", "");
//		
//		assertTrue(loginPage.usernameError().getText().equals("Korisničko ime obavezno"));
//		assertTrue(loginPage.passwordError().getText().equals("Lozinka obavezna"));
//		assertTrue(loginPage.getSignInBtn().isDisplayed());
//		assertFalse(loginPage.getSignInBtn().isEnabled());
		
//		assertTrue(sideMenuPage.getInstitucijaBtn().isDisplayed());
//		
//		sideMenuPage.getInstitucijaBtn().click();
//		sideMenuPage.getIstrazivaciBtn().click();
//		topMenuPage.getJeziciBtn().click();
//		languagesPage.srpskiCir().click();
//		topMenuPage.getJeziciBtn().click();
//		languagesPage.engleski().click();
//		topMenuPage.getJeziciBtn().click();
//		languagesPage.srpskiLat().click();
//		topMenuPage.getUserMenuBtn().click();
//		
//		assertTrue(userMenuPage.getUputstvoZaKoriscenje().isDisplayed());
//		assertTrue(userMenuPage.getKontaktirajteNas().isDisplayed());
//		assertTrue(userMenuPage.getPromenaLozinke().isDisplayed());
//		assertTrue(userMenuPage.getOdjava().isDisplayed());
		
//		institucija
		
//		assertTrue(institucijaMenuPage.osnovniPodaci().isDisplayed());
//		assertTrue(institucijaMenuPage.podaciZaProjekte().isDisplayed());
//		assertTrue(institucijaMenuPage.podaciZaRegistar().isDisplayed());
//		assertTrue(institucijaMenuPage.istrazivaci().isDisplayed());
		
//		osnovniPodaciPage.unesiOsnovnePodatke("Nemanja Žunić", "Institution Nemanja Zunic", "Srbija", "Novi Sad", "Novi Sad", "Narodnog Fronta 21", "www.nz.com", "nemanja.zunic1@gmail.com", "44444", "NZ");
//		osnovniPodaciPage.unesiOsnovnePodatkeSaNovomDrzavom("Nemanja Žunić", "Institution Nemanja Zunic", "Nova", "Drzava", "Novi Sad", "Novi Sad", "Narodnog Fronta 21", "www.nz.com", "nemanja.zunic1@gmail.com", "44444", "NZ");
//		buttonsPage.getSaveBtn().click();
		
//		institucijaMenuPage.podaciZaRegistar().click();
				
//		institucijaMenuPage.istrazivaci().click();
//		institucijaIstrazivaciPage.angazujOsobu().click();
//		angazujOsobuPage.angazujOsobu("Nemanja Zunic", "Asistent", "17.11.2016", "20.11.2016");
//		buttonsPage.getSaveBtn().click();

//		institucijaMenuPage.istrazivaci().click();
//		assertTrue(institucijaIstrazivaciPage.getIstrazivacRowByName("Nemanja Žunić").isDisplayed());
//		institucijaIstrazivaciPage.deleteIstrazivacByName("Nemanja Žunić");
//		assertFalse(institucijaIstrazivaciPage.getIstrazivacRowByName("Nemanja Žunić").isDisplayed());
	}
	
	@AfterSuite
	public void exit() {
		driver.close();
	}
	
}
